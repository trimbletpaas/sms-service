stage { 'configure': require => Stage['main'] }
stage { 'deploy': require => Stage['configure'] }

node basenode {
    $depsync_svn_repo 	= "https://svn.trimblepaas.com/repo"
	$deploy_new_packs	= "true"
	$local_package_dir  = "/paas/soft/wso2"
    $package_repo      = "http://deployer.trimblepaas.com:8080"
	class { wso2base:
          stage => configure,
       }
}

node confignode inherits basenode  {
	## Service subdomains
	$domain               	= 'trimblepaas.com'
	$stratos_domain		= "trimblepaas.com"	
	$as_subdomain		= "appserver"
	$as2_subdomain		= "appserver2"	
	$is_subdomain		= "identity"
	$am_subdomain		= "api"
    $esb_subdomain		= "esb"
    $elb_subdomain		= "wlb"
	$management_subdomain 	= "mgt"
	$gateway_subdomain      = 'gateway'
	$keymanager_subdomain   = 'keymanager'
	
	## DB configuration details
    $enablemysql		= true
    $max_active 		= "300"
	$max_wait 			= "60000"

	## Oracle server configuration details
	$oracle_server_1 	= "tpaas.cacqxtrv7hbg.eu-west-1.rds.amazonaws.com"
	$oracle_port 		= "1521"
	

     ## MySQL server configuration details
    # $mysql_server		="identity-stage.cfjhwqynhfso.us-west-2.rds.amazonaws.com"
     #$mysql_server		=hiera("is_mysql_server")
     #$mysql_port		=hiera("is_mysql_port")
     #$mysql_username		=hiera("is_mysql_username")
     #$mysql_password		=hiera("is_mysql_password")

     $admin_username		= "admin"
	 $admin_password		= "KQ2A58NUO1p0z1I"
	
	 #$is_mysql_server   = "identity-stage-ro-api-stg.cfjhwqynhfso.us-west-2.rds.amazonaws.com"
     #$is_mysql_server   = hiera("is_mysql_server")
#	 $is_mysql_port     = hiera("is_mysql_port")
#	 $is_mysql_username = hiera("is_mysql_username")
#	 $is_mysql_password = hiera("is_mysql_password")

	 ## *************** Main flags to switch/control the products functionality ****** 
     ## Specific purpose switch Deatils
     $identity_deployment = true
	 
	 ## Identity Service nodes should be having this flag as true.
	 $unpack_wars = false
	 
	 ## This is use for API stack.
	 $shared_services_deployment = false

 #   $base64_token               =hiera("base64_token")
     $saml_oauth_client          ='Qn5DQHCYfshxeZh6R9SL1HM2lsMa'
     $saml_oauth_secret          ='cbkAs1gajdwPAMbrSR54hPAIcz0a'
 
	
     ##User-mgt configuration details
     ##if user-store is read-only ldap,set usermgt ->readonly_ldap;if the user-store is r/w mode ldap then set usermgt ->readwrite_ldap
     $usermgt 		= 'jdbc'
	
     #LDAP settings 
  #   $ldap_connection_uri      = hiera("ldap_connection_uri")
     $bind_dn                  = 'cn=Manager,dc=idm,dc=com'
     #$bind_dn_password         = hiera("bind_dn_password")
     $user_search_base         = 'ou=system'
     $group_search_base        = 'ou=system'
     $sharedgroup_search_base  = 'ou=SharedGroups,dc=wso2,dc=org'	
     $password_hash_method     = 'PLAIN_TEXT'
     $username_list_filter     = '(objectClass=person)'
     $user_entry_objectclass   = 'identityPerson'
     $username_search_filter   = '(&amp;(objectClass=person)(uid=?))'
     $username_attribute       = 'uid'
     $read_groups              = 'true'
     $groupname_list_filter    = '(objectClass=groupOfNames)'
     $groupname_search_filter   = '(&amp;(objectClass=groupOfNames)(cn=?))'
     $groupname_attribute      = 'cn'
     $sharedgroupname_attribute = 'cn'
     $sharedgroupname_list_filter = '(objectClass=groupOfNames)'
     $sharedgroupname_search_filter = '(&amp;(objectClass=groupOfNames)(cn=?))'
     $sharedtenant_list_filter = '(objectClass=organizationalUnit)'
     $sharedgroup_entry_objectclass = 'groupOfNames'
     $sharedtenant_attribute = 'ou'
     $sharedtenant_objectclass = 'organizationalUnit'
     $membership_attribute = 'member'
     $user_roles_cache_enabled = 'true'
     $write_groups = 'true'
     $group_entry_objectclass = 'groupOfNames'	

	#AWS details
	$aws_access_key			= 'AKIAIJYTC7WN2HSQAEQA'
	$aws_secret_access_key	= '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'
	$aws_account_id			= '531988440191'
	$aws_availability_zone	= 'eu-west-1b'
	$aws_security_group_id	= 'sg-516b7d33'
	$aws_instance_type		= 'm1.large'
	$aws_keypair			= 'tpaas-balaji'
	$aws_subnet_id			= 'subnet-69ece40b'
	$aws_ami_id             = 'ami-59c1332e'
#	$aws_availability_zone_2  = 'eu-west-1a'
	
	#AWS ELB details
        #AWS ELB details          
   #     $is_endpoint              =hiera("is_endpoint")
    #    $gateway_endpoint          =hiera("gateway_endpoint")
     #   $gateway_elb_subdomain    =hiera("gateway_elb_subdomain")
#        $keymanager_elb_subdomain =hiera("keymanager_elb_subdomain")
#        $keymanager_is_subdomain   =hiera("keymanager_is_subdomain")
 #       $gateway_elb               =hiera("gateway_elb")
  #      $keymanager_elb           =hiera("keymanager_elb")
   #     $is_elb                    =hiera("is_elb")
                              
    #    $saml_sso_url              =hiera("saml_sso_url")
     #   $identity_url             =hiera("identity_url")
                              
                                  	
	$tpaas_admin_username     = 'internalapiadmin'	
	$tpaas_admin_password     = 'Trimb1e@Api'
$DenvironmentName         = hiera('DenvironmentName')
$DB = hiera('DB')
$DBUsername = hiera('DBUsername')
$DBPassword = hiera('DBPassword')
}

node /^app(\d+)?\.trimblepaas\.com$/ inherits confignode {
        $server_ip      = $ipaddress
	             
	class {'appserver':
                version      		=> '5.2.1',
                offset          	=> 0,
                hazelcast_port  	=> 4000,
                config_db       	=> 'appserver_config',
                maintenance_mode    	=> 'zero',
                depsync                 => false,
                sub_cluster_domain  	=> 'worker',            
                clustering       		=> false,                
                owner                   => 'tpaas',
                group                   => 'tpaas',
                monitoring          	=> false,
                members             	=> {'127.0.0.1' => 4200 },
                target                  => '/paas/wso2/AS_W',
                cloud                   =>  false,
	}
}

#!/bin/bash

INDEX="/tmp/list"
WGET=`which wget`
ARTILINK=`cat /etc/artiurl`

if [ -z "$ARTILINK" ] 
then
ARTILINK="http://artifactory.trimblepaas.com/artifactory/libs-snapshot-local/com/trimble/sms/1.0.1-SNAPSHOT/"
fi

$WGET -O- $ARTILINK > $INDEX

war=`grep "war<" $INDEX | tail -n1 | awk -F">" '{print $(NF-1)}' | sed 's@\/a@@g'  | tr -d '<>,'`

echo $war >> /tmp/wardetail.txt

$WGET ${ARTILINK}${war}

if [ "$war" != "sms-1.0.war" ]
then
mv $war sms-1.0.war
fi

# ----------------------------------------------------------------------------
#  Copyright 2005-2013 WSO2, Inc. http://www.wso2.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ----------------------------------------------------------------------------
#
# Initializing the deployment

define appserver::initialize ($repo, $version, $service, $local_dir, $target, $mode, $owner,) {

  file { "/tmp/fetchwar":
    ensure       => present,
    owner        => $owner,
    sourceselect => all,
    recurse      => true,
    source       => [
      'puppet:///modules/appserver/scripts/']
  }

  exec {
    "creating_target_for_${name}":
      path    => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
      command => "mkdir -p ${target}";

    "downloading_wso2${service}-${version}.zip_for_${name}":
      path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
      cwd       => $local_dir,
      unless    => "test -f ${local_dir}/wso2${service}-${version}.zip",
      command   => "wget -q ${repo}/wso2${service}-${version}.zip",
      logoutput => 'on_failure',
      creates   => "${local_dir}/wso2${service}-${version}.zip",
      timeout   => 0,
      require   => Exec["creating_target_for_${name}"];

    "extracting_wso2${service}-${version}.zip_for_${name}":
      path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
      cwd       => $target,
      command   => "unzip ${local_dir}/wso2${service}-${version}.zip",
      logoutput => 'on_failure',
      timeout   => 0,
      require   => Exec["downloading_wso2${service}-${version}.zip_for_${name}"];

    "setting_permission_for_${name}":
      path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
      cwd       => $target,
      command   => "chown -R ${owner}:${owner} ${target}/wso2${service}-${version} ;
                    chmod -R 755 ${target}/wso2${service}-${version}",
      logoutput => 'on_failure',
      timeout   => 0,
      require   => Exec["extracting_wso2${service}-${version}.zip_for_${name}"];

     "downloading_jdk7.tgz_for_${name}":
      path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
      cwd       => $local_dir,
      unless    => "test -f ${local_dir}/jdk7.tgz",
      command   => "wget -q ${repo}/jdk7.tgz",
      logoutput => 'on_failure',
      creates   => "${local_dir}/jdk7.tgz",
      timeout   => 0,
      require   => Exec[
                        "creating_target_for_${name}"];

     "extracting_jdk7.tgz_for_${name}":
      path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
      cwd       => '/paas/java',
      unless    => "test -d /paas/java/jdk1.7.0_75",
      command   => "tar -xzf ${local_dir}/jdk7.tgz",
      logoutput => 'on_failure',
      creates   => "${target}/jdk1.7.0_75",
      timeout   => 0,
      require   => Exec["downloading_jdk7.tgz_for_${name}"];

     "creating_directory":
      path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
      command   => "mkdir -p /paas/deployment/",
      require   => Exec["extracting_jdk7.tgz_for_${name}"];

     "getting_smsapi_war":
     path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
     cwd       => '/paas/deployment/',
     command   => "bash /tmp/fetchwar/fetchwar.sh",
     logoutput => 'on_failure',
     timeout   => 0,
     require   => Exec["creating_directory"];

     "setting_permissions_forwar":
     path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
     cwd       => '/paas/deployment/',
     command   => "chown tpaas:tpaas sms-1.0.war;chown -R tpaas:tpaas /paas/wso2/",
     require   => Exec["getting_smsapi_war"];

     "copyingwarfile_server":
     path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
     cwd       => '/paas/deployment/',
     command   => "cp -r sms-1.0.war ${target}/wso2${service}-${version}/repository/deployment/server/webapps/",
     require   => Exec["setting_permissions_forwar","setting_permission_for_${name}"];

#     "copying_env_file":
#     path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
#     cwd       => '/paas/deployment/',
#     command   => "cp -r apicloud_uienvironments.json  /paas/wso2/AS_W/wso2as-5.2.1/repository/conf/",
#     require   => Exec["copyingwarfile_server"];

     #"importcertificate_tdc":
     #path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
     #cwd       => '/paas/wso2/AS_W/wso2as-5.2.1/repository/resources/security/',
     #command   => 'openssl s_client -showcerts -connect tdc-qa1.trimblepaas.com:443 </dev/null 2>/dev/null|openssl x509 -outform PEM > cert.pem;/paas/java/jdk1.6.0_45/bin/keytool -importcert -alias tdc-qa1.trimblepaas.com  -keystore client-truststore.jks -storepass wso2carbon -noprompt  --file cert.pem',
     #require   =>Exec["copyingwarfile_server"];

  }
}

#
class wso2base::users {
    $users = ["kurumba"]

    user { $users:
        ensure  => present,
        managehome => true,
    }
}

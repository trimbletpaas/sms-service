# Class: wso2base
#
# Full description of class wso2base here.
#
# Examples
#
#  class { wso2base:
#    java => 'false',
#  }
#
# Authors
#
# Thilina Piyasundara <mail@thilina.org>
#
class wso2base (
    ) inherits params {

   
    
    class { hosts : }
    class { system_config :}   
    #->
    #class { java :}
    #->
    #class { maven :}
}

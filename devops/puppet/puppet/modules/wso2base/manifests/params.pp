#

class wso2base::params {

    $domain               = 'trimblepaas.com'
    $package_repo         = "http://downloads.${domain}"
    $local_package_dir    = '/mnt/packs'
$hosts_map = hiera('hosts')

    $hosts_mapping = [
					 	"127.0.0.1,localhost.localdomain localhost",
                        "::1,localhost6.localdomain6",

                        "10.94.12.10,web.trimblepaas.com web identity-stg.trimble.com web01 status.identity-stg.trimble.com",
						"10.94.12.11,web02",
						
						"10.94.12.39,is1.trimblepaas.com is1          # IS-SMAL-SSO-MGR01",
                        "10.94.12.60,is2.trimblepaas.com is2          # IS-SMAL-SSO-MGR02",
						
						"10.94.12.45,is-svc1.trimblepaas.com is-svc1  # IS-SVC-MGR01",
						"10.94.12.46,is-svc2.trimblepaas.com is-svc2  # IS-SVC-MGR02",
						
						"10.94.12.42,app1.trimblepaas.com app1",
						"10.94.12.36,app2.trimblepaas.com app2",
						
                        "10.94.12.37,deployer.trimblepaas.com deployer # Puppet node",
						
						"10.94.12.87,ldap1.idm.com ldap1 # LDAP CL1",
						"10.94.12.89,ldap2.idm.com ldap2 # LDAP CL2",
						"10.94.12.70,mongodb.trimblepaas.com mongodb   # Mongo DB Server",
                    ]
}

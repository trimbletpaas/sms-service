# ----------------------------------------------------------------------------
#  Copyright 2005-2013 WSO2, Inc. http://www.wso2.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ----------------------------------------------------------------------------
class system_config {

	include hosts

	user { "tpaas": 
  		password   => "paas123",
  		ensure     => present,                            
 	 	managehome => true,
	}

	file {  "/root/bin":
                owner   => root,
                group   => root,
                ensure  => "directory";

		"/root/bin/puppet_init.sh":
                owner   => root,
                group   => root,
                mode    => 0755,
                source  => "puppet:///modules/commons/bin/puppet_init.sh";

		"/root/bin/puppet_clean.sh":
                owner   => root,
                group   => root,
                mode    => 0755,
                content => template("puppet_clean.sh.erb"),
        }

        Exec {  path    => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/java/bin/"  }
}


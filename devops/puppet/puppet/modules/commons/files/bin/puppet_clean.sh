#!/bin/bash
#
# This will clean the entire Stratos system from the server.
# Once run, you need to re-run the puppet_init.sh to recover
# the setup.
#

#PACKS_DIR=/paas/soft/wso2/
STRATOS_DIR=/paas/wso2
ARCHIVE_DIR=/paas/archive
WHICH=`which which`
RM=`which rm`
KILL=`which kill`
PKILL=`which pkill`
CAT=`which cat`
FIND=`which find`
ECHO=`which echo`
MKDIR=`which mkdir`
RSYNC=`which rsync`
DATE=`which date`
BASENAME=`which basename`

DATE=`${DATE} +%Y%m%d%H%M`
CHKCONFIG=`${WHICH} chkconfig`

function _archive_logs {
       OP=${1}
         if [ -d ${ARCHIVE_DIR}/${OP} ]; then
                ${RM} -rf ${ARCHIVE_DIR}/${OP}
        fi

        if [ ${OP} == "all" ]; then
          for S in `${FIND} ${STRATOS_DIR} -type d -iname "wso2*-*.*" -exec ls -d {} \;`
 do                     DIRNAME=`$BASENAME ${S}` 
                        ${MKDIR} -p ${ARCHIVE_DIR}/${OP}/${DATE}/
                        ${ECHO} -e "\nDumping ${S} to ${ARCHIVE_DIR}/${OP}/${DATE}/${DIRNAME}"
                        ${RSYNC} -arz ${S}/repository/logs ${ARCHIVE_DIR}/${OP}/${DATE}/${DIRNAME}
                done
        else
                SERVICE=`${FIND} ${STRATOS_DIR} -type d -iname "wso2${OP}-*.*" -exec ls -d {} \;`

                ${MKDIR} -p ${ARCHIVE_DIR}/${DATE}/
                ${ECHO} -e "\nDumping ${SERVICE} to ${ARCHIVE_DIR}/${DATE}/${OP}/"
                ${RSYNC} -arz ${SERVICE}/repository/logs ${ARCHIVE_DIR}/${DATE}/${OP}
        fi

}

if [ $# -eq 0 ]; then

        ${ECHO} -e "\nCleaning ${STRATOS_DIR}"

       _archive_logs all

        for PID in `find ${STRATOS_DIR} -type f -name "wso2carbon.pid" -exec ${CAT} {} \;`
        do
                ${KILL} -9 ${PID}
#                ${RM} -rf ${STRATOS_DIR}/*
#               ${RM} -rf ${PACKS_DIR}/*
        done
else
   for SERVICE in $@
  do
                 SERVICE_DIR=`${FIND} ${STRATOS_DIR} -type d -iname "wso2${SERVICE}-*.*" -exec ls -d {} \;`

                if [ ! -d ${SERVICE_DIR} ]; then
                        ${ECHO} -e "\nNo deployment found for ${SERVICE}"
                else
                        PID=`${CAT} ${SERVICE_DIR}/wso2carbon.pid`
                        _archive_logs ${SERVICE}
                        ${ECHO} -e "\nRemoving ${SERVICE} with the pid ${PID}"
                        ${KILL} -9 ${PID}
                        ${RM} -rf ${SERVICE_DIR}
                fi

        done
fi

${CHKCONFIG} --del wso2

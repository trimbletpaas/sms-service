curl  -X POST -H "Content-type: application/json" \
-d '{
      "graphs" : [{
          "title": "JMX THREAD COUNT",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:jmx.thread_count{*}"}
              ]
          },
          "viz": "timeseries"
      },
	  {
          "title": "JMX SYSTEM LOAD AVERAGE",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:jmx.system_load_average{*}"}
              ]
          },
          "viz": "timeseries"
      },
	  {
          "title": "NETWORK BYTES SENT",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:system.net.bytes_sent{*}"}
              ]
          },
          "viz": "timeseries"
      },
	  {
          "title": "JVM HEAP MEMORY",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "max:jvm.heap_memory{*}"}
              ]
          },
          "viz": "timeseries"
      },
	  {
          "title": "JMX OPEN FILE DESCRIPTOR COUNT",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:jmx.open_file_descriptor_count{*}"}
              ]
          },
          "viz": "timeseries"
      },
	  {
          "title": "JMX MAX FILE DESCRIPTOR COUNT",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:jmx.max_file_descriptor_count{*}"}
              ]
          },
          "viz": "timeseries"
      },
	  {
          "title": "SYSTEM DISK FREE",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:system.disk.free{*}"}
              ]
          },
          "viz": "timeseries"
      },
	  {
          "title": "SYSTEM MEM FREE",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:system.mem.free{*}"}
              ]
          },
          "viz": "timeseries"
      },
	  {
          "title": "SYSTEM CPU USAGE",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:system.cpu.system{*}"}
              ]
          },
          "viz": "timeseries"
      }],
      "title" : "APP SERVER %instancename%",
      "description" : "A dashboard with JMX AND SYSTEM info.",
      "template_variables": [{
          "name": "%instancename%",
          "prefix": "%instancename%",
          "default": "host:%instancename%"
      }]
    }' \
"https://app.datadoghq.com/api/v1/dash?api_key=6f69ec9a868ae311a58c59a9f73fb3ca&application_key=5655b8b4217a6ad8c08811557c0492b262b07548"

EMAIL=$EmailAdd
api_key="6f69ec9a868ae311a58c59a9f73fb3ca"
#app_key="a7d65e5521b5fa34c7219014ea6d0c01f68bbf5c"
app_key="b64f7dbdc0f7db7aa51828ba7b88af023dba610a"

#system.net.bytes_sent
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "avg(last_5m):avg:system.net.bytes_sent{host:%instancename%} > 530000000",
      "name": "NetworkOut-CRITICAL ",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"
#Jvm.heap_memory
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "avg(last_5m):max:jvm.heap_memory{host:%instancename%} > 3890000000",
      "name": "Java heap memory ",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"
#system.disk.free
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "avg(last_5m):avg:system.disk.free{host:%instancename%} < 4000000000",
      "name": "Disk space",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"

#system.mem.free
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "min(last_5m):avg:system.mem.free{host:%instancename%} < 2048",
      "name": "Memory free ",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"
#system.cpu.system
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "min(last_5m):avg:system.cpu.system{host:%instancename%} > 30",
      "name": "CPU utilization",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"


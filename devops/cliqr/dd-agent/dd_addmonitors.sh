. /usr/local/osmosix/etc/userenv
ENV=$environment

EMAIL="TPaaS_NOC@grpmail.trimble.com"
api_key="6f69ec9a868ae311a58c59a9f73fb3ca"
#app_key="a7d65e5521b5fa34c7219014ea6d0c01f68bbf5c"
#app_key="b64f7dbdc0f7db7aa51828ba7b88af023dba610a"
app_key="5655b8b4217a6ad8c08811557c0492b262b07548"

#jmx.thread_count
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "avg(last_5m):avg:jmx.thread_count{host:%instancename%} > 2000",
      "name": "Thread count",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"
#jmx.sytem_load_average
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "avg(last_5m):avg:jmx.system_load_average{host:%instancename%} > 4",
      "name": "System load average",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"
#system.net.bytes_sent
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "avg(last_5m):avg:system.net.bytes_sent{host:%instancename%} > 530000000",
      "name": "NetworkOut-CRITICAL ",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"
#Jvm.heap_memory
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "avg(last_5m):max:jvm.heap_memory{host:%instancename%} > 3890000000",
      "name": "Java heap memory ",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"
#jmx.open_file_descriptor_count
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "avg(last_5m):avg:jmx.open_file_descriptor_count{host:%instancename%} > 50096",
      "name": "JMX-open file descriptor count",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"
#jmx.max_file_descriptor_count
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "avg(last_5m):avg:jmx.max_file_descriptor_count{host:%instancename%} > 150096",
      "name": "JMX-max file descriptor count",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"

#system.disk.free
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "avg(last_5m):avg:system.disk.free{host:%instancename%} < 4000000000",
      "name": "Disk space",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"

#system.mem.free
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "min(last_5m):avg:system.mem.free{host:%instancename%} < 2048",
      "name": "Memory free ",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"
#system.cpu.system
curl -X POST -H "Content-type: application/json" \
-d '{
      "type": "metric alert",
      "query": "min(last_5m):avg:system.cpu.system{host:%instancename%} > 30",
      "name": "CPU utilization",
      "message": '\"@$EMAIL\"',
      "options": {
      	"notify_no_data": true,
      	"no_data_timeframe": 20
      }
    }' \
    "https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"


#port check
curl -X POST -H "Content-type: application/json" \
-d '
{
     "type": "service check",
    "query": "\"tcp.can_connect\".over(\"host:%instancename%\",\"instance:sms_service_staus_%instancetagname%\").last(6).count_by_status()",
     "name": "Email Service Check",
     "message":  '\"@$EMAIL\"',
     "tags": ["project:sms"],
     "options": {
       "notify_no_data": false,
       "multi": true,
       "no_data_timeframe": 20,
       "thresholds" : {
       "critical" : 5
       }
        }
}' \
"https://app.datadoghq.com/api/v1/monitor?api_key=${api_key}&application_key=${app_key}"

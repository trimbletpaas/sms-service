#!/bin/bash

DATADOGCONFFILE="/etc/dd-agent/datadog.conf"
NEW_DOMAIN_NAME=trimblepaas.com
. /usr/local/osmosix/etc/userenv
#BASE_DIR="/opt/remoteFiles/appPackage/email"
BASE_DIR="/opt/remoteFiles/appPackage/devops/cliqr/"
#instance=`echo "$servicename-$environment"`
HOSTNAME="`hostname`"
TYPE=`echo ${HOSTNAME} |  awk -F"." '{print $1}'`
TAGNAME="Email-${environment}"
FILELOCATION="$BASE_DIR"
instance=`echo "$servicename-$TYPE-$environment-$NEW_DOMAIN_NAME"`
INSTANCETAGNAME=`echo "$servicename-$TYPE-$environment-status"`

replaceToken() {
        sed -i "s,$2,$3,g" $1
}
DD_API_KEY=6f69ec9a868ae311a58c59a9f73fb3ca bash -c "$(curl -L https://raw.githubusercontent.com/DataDog/dd-agent/master/packaging/datadog-agent/source/install_agent.sh)"
echo "Agent Installed********************************************"
/etc/init.d/datadog-agent stop
echo "Agent stopped**********************************************"

#change the hostname
echo  "hostname : ${instance}" >> $DATADOGCONFFILE
echo "Hostname Changed************************************************************"

#goto confd location and add the yaml files

#replaceToken $FILELOCATION/dd-agent/yaml/http_check.yaml %email% $cliqrOwner
#replaceToken $FILELOCATION/dd-agent/yaml/http_check.yaml %instancename% $instance
replaceToken $FILELOCATION/dd-agent/yaml/jmx.yaml %instancename% $instance
replaceToken $FILELOCATION/dd-agent/yaml/tcp_check.yaml %instancename% $instance
replaceToken $FILELOCATION/dd-agent/yaml/tcp_check.yaml %instancetagname% $TYPE

replaceToken $FILELOCATION/dd-agent/dd_addmonitors.sh %instancename% $instance
replaceToken $FILELOCATION/dd-agent/dd_addmonitors.sh  %instancetagname% $TYPE 
replaceToken $FILELOCATION/dd-agent/dd_db_app.sh %instancename% $instance
#replaceToken $FILELOCATION/dd-agent/dd_db_app.sh  %instancehostname% $HOSTNAME

#cp $FILELOCATION/dd-agent/yaml/http_check.yaml /etc/dd-agent/conf.d/
cp $FILELOCATION/dd-agent/yaml/jmx.yaml /etc/dd-agent/conf.d/
cp $FILELOCATION/dd-agent/yaml/tcp_check.yaml /etc/dd-agent/conf.d/
echo " YAML FILES ADDED************************************************"
echo "api_key: 6f69ec9a868ae311a58c59a9f73fb3ca" >>$DATADOGCONFFILE

#start the datadog 
/etc/init.d/datadog-agent start
echo "Agent Started************************************************"





curl  -X POST -H "Content-type: application/json" \
-d '{
   
	  "graphs" : [{
          "title": "NETWORK BYTES SENT",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:system.net.bytes_sent{*}"}
              ]
          },
          "viz": "timeseries"
      },
      {
	      "title": "SYSTEM DISK FREE",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:system.disk.free{*}"}
              ]
          },
          "viz": "timeseries"
      },
      {
          "title": "SYSTEM MEM FREE",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:system.mem.free{*}"}
              ]
          },
          "viz": "timeseries"
      },
      {
          "title": "SYSTEM CPU USAGE",
          "definition": {
              "events": [],
              "requests": [
                  {"q": "avg:system.cpu.system{*}"}
              ]
          },
          "viz": "timeseries"
      }],
      "title" : "WEB SERVER %instancename%",
      "description" : "A dashboard with JMX AND SYSTEM info.",
      "template_variables": [{
          "name": "%instancename%",
          "prefix": "%instancename%",
          "default": "host:%instancename%"
      }]
    }' \
"https://app.datadoghq.com/api/v1/dash?api_key=6f69ec9a868ae311a58c59a9f73fb3ca&application_key=b64f7dbdc0f7db7aa51828ba7b88af023dba610a"

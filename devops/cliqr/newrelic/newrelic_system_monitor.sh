#! /bin/bash

RPM=`which rpm`
cd /opt
sudo ${RPM}  -Uvh http://download.newrelic.com/pub/newrelic/el5/i386/newrelic-repo-5-3.noarch.rpm
sudo yum install -y newrelic-sysmond
sudo nrsysmond-config --set license_key=3bf1e433e81200c59a13755f978e6a59a5fb59ad
sudo /etc/init.d/newrelic-sysmond start
sudo chkconfig newrelic-sysmond on

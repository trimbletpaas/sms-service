#!/bin/bash

CLIQRPATH="/opt/remoteFiles/appPackage/devops/cliqr"
PUPPETPATH="/opt/remoteFiles/appPackage/devops/puppet"

# Exporting Env to Deployer
touch  /tmp/$$.txt
env | grep -v Key > /tmp/$$.txt 
scp -pr -o StrictHostKeyChecking=no /tmp/$$.txt tpaas_admin@$DeployerIP:/var/tmp/envs/

# Exporting PuppetChanges to Deployer
cd $PUPPETPATH 
tar -zcvf ${servicename}.tgz *
ssh -o StrictHostKeyChecking=no -t tpaas_admin@$DeployerIP "mkdir -p /paas/scripts/tmp/ /paas/puppettemplates/ ; rm -rvf /paas/scripts/tmp/*"
scp -pr -o StrictHostKeyChecking=no $PUPPETPATH/${servicename}.tgz tpaas_admin@$DeployerIP:/paas/puppettemplates/
scp -pr -o StrictHostKeyChecking=no $CLIQRPATH/scripts/config${servicename}Service.sh tpaas_admin@$DeployerIP:/paas/scripts/tmp/
ssh -o StrictHostKeyChecking=no -t tpaas_admin@$DeployerIP "sudo /paas/scripts/tmp/config${servicename}Service.sh $servicename $environment $$.txt &> /paas/cliqrtmp/${servicename}/logs/${parentJobId}-config.log"
#ssh -o StrictHostKeyChecking=no -t tpaas_admin@$DeployerIP "sleep 1000"

exit 0

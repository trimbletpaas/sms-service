#!/bin/bash

CLIQRPATH="/opt/remoteFiles/appPackage/devops/cliqr"
PUPPETPATH="/opt/remoteFiles/appPackage/devops/puppet"

touch  /tmp/$$.txt
env | grep -v Key > /tmp/$$.txt 
scp -pr  /tmp/$$.txt tpaas_admin@$DeployerIP:/var/tmp/envs/

scp -pr -o StrictHostKeyChecking=no $CLIQRPATH/scripts/cleanup${servicename}Service.sh /paas/scripts/tmp/
ssh -o StrictHostKeyChecking=no -t tpaas_admin@$DeployerIP "sudo /paas/scripts/tmp/cleanup${servicename}Service.sh $servicename $environment $$.txt &> /paas/cliqrtmp/email/logs/${parentJobId}-cleanup.log"

exit 0

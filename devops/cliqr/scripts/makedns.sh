#!/bin/bash

ENV="${environment}-cliqr${parentJobId}"
ELBENDPOINT="${servicename}-${environment}-int.${internaldomainname}"
DOMAINNAME="trimblepaas.com"

SCRIPT="/opt/remoteFiles/appPackage/devops/cliqr/scripts"

#installing dependent packages

yum install epel-release -y
yum -y install python-pip
pip install boto

if [ $CliqrDepEnvName == "TPaaS-Dev-Env" ]
then
$SCRIPT/dns.py $CliqrCloudAccountPwd $CliqrCloud_AccessSecretKey $HostedZoneID $internaldomainname $ELBENDPOINT $CliqrTier_appelb_IP $DNSRole "cliqrDnsSession"
fi

if [ $CliqrDepEnvName == "TPaaS-Prod-Env" ]
then
$SCRIPT/dns.py $CliqrCloudAccountPwd $CliqrCloud_AccessSecretKey $HostedZoneID $internaldomainname $ELBENDPOINT $CliqrTier_appelb_IP 
fi

exit 0

#!/bin/bash -x

replaceToken() {
        sed -i "s,$2,$3,g" $1
}


SVCNAME=$1
ENVNAME=$2
USERENV=`echo /var/tmp/envs/$3`

. $USERENV

SVCNAME=${servicename}
TMPDIR="/paas/cliqrtmp"
TEMPLATEDIR="/paas/puppettemplates"
PUPPETDIR="/paas/puppet/tpaas"
PUPPETCONF="/etc/puppet/puppet.conf"
HIERADIR="/var/lib/hiera"
ENV="${ENVNAME}cliqr${parentJobId}"
PUPPETENV="tpaas${SVCNAME}${ENV}"


addHost () {
        HOSTTYPE=$1
        var=CliqrTier_${HOSTTYPE}_IP
        if [ ! -z ${!var} ]
        then
                echo "- ${!var}, $HOSTTYPE.${domainName} $HOSTTYPE," >> yaml/$PUPPETENV.yaml
        fi
}

#if [ $CliqrDepEnvName == "TPaaS-Prod-Env" ]
#then
#RDSDOMAINNAME=".cfjhwqynhfso.us-west-2.rds.amazonaws.com"
#fi
#if [ $CliqrDepEnvName == "TPaaS-Dev-Env" ]
#then
#RDSDOMAINNAME=".cdryrulibdcx.us-west-2.rds.amazonaws.com"
#fi

#if [ -z "$CliqrTier_aws_rds_HOSTNAME" ]
#then
#	DBJOBID=`echo "$parentJobId + 2" | bc -l `
#	if [ -z "$DBEndpoint" ]
#	then
#		CliqrTier_aws_rds_HOSTNAME="cliqr-job-instance-${DBJOBID}${RDSDOMAINNAME}"
#	else
#		CliqrTier_aws_rds_HOSTNAME="$DBEndpoint"
#	fi	
#fi	

if [  -z "$DBUserName" ]
then
	DBUserName="root"
fi
if [  -z "$DBPassword" ]
then
        DBPassword="Password123"
fi


mkdir -p $TMPDIR/$SVCNAME/$parentJobId
cp $TEMPLATEDIR/$SVCNAME.tgz $TMPDIR/$SVCNAME/$parentJobId/$ENV.tgz

cd $TMPDIR/$SVCNAME/$parentJobId
tar -zxvf $ENV.tgz
rm -rf $ENV.tgz

# Configuring Puppet scripts
mkdir -p $PUPPETDIR/$SVCNAME/cliqrstacks/$ENV
mv puppet/* $PUPPETDIR/$SVCNAME/cliqrstacks/$ENV/

#Configuring Puppet conf
replaceToken conf/puppet.conf %env% $ENV
echo "" >> $PUPPETCONF
cat conf/puppet.conf >> $PUPPETCONF

#Configuring Yaml Datasource
mv yaml/yaml yaml/$PUPPETENV.yaml
replaceToken yaml/$PUPPETENV.yaml %env% $ENV
replaceToken yaml/$PUPPETENV.yaml %DB% $DBEndpoint
replaceToken yaml/$PUPPETENV.yaml %DBUsername% $DBUserName
replaceToken yaml/$PUPPETENV.yaml %DBPassword% $DBPassword
replaceToken yaml/$PUPPETENV.yaml %app1ip% $CliqrTier_app1_IP
addHost app2
cp yaml/$PUPPETENV.yaml $HIERADIR/

#cleanup
rm -rf $USERENV

#!/usr/bin/python
import boto,sys
import boto.sts
from boto.route53.record import ResourceRecordSets
from boto.route53.connection import Route53Connection


def tpaasassumerole(AccessKey,SecretKey,RoleName,SessionName):
    STSConnobj = boto.sts.STSConnection(aws_access_key_id=AccessKey,aws_secret_access_key=SecretKey)
    cred = STSConnobj.assume_role(RoleName,SessionName)
    return cred.credentials.access_key,cred.credentials.secret_key,cred.credentials.session_token

def tpaasmakealiasdns(AccessKey,SecretKey,HostedZoneName,DomainName,DNSName,AliasName,SessionToken=None):
    route53 = Route53Connection(AccessKey, SecretKey,security_token=SessionToken)
    hid = HostedZoneName
    mydict = route53.get_all_hosted_zones("", "")
    for HZs in  mydict.ListHostedZonesResponse.HostedZones:
        print HZs.Name
        if HZs.Name == DomainName+".":
            hostedzone = HZs.Id.split('/')[-1]
            print hostedzone
    changes = ResourceRecordSets(route53, hostedzone)
    change = changes.add_change("UPSERT", DNSName, "A",alias_hosted_zone_id=HostedZoneName,alias_dns_name=AliasName,alias_evaluate_target_health="false")
    result = changes.commit()
    return result

def tpaasmakecnamedns(AccessKey,SecretKey,HostedZoneName,DomainName,DNSName,CNameVal,SessionToken=None):
    route53 = Route53Connection(AccessKey, SecretKey,security_token=SessionToken)
    hid = HostedZoneName
    mydict = route53.get_all_hosted_zones("", "")
    for HZs in  mydict.ListHostedZonesResponse.HostedZones:
        print HZs.Name
        if HZs.Name == DomainName+".":
            hostedzone = HZs.Id.split('/')[-1]
            print hostedzone
    changes = ResourceRecordSets(route53, hostedzone)
    change = changes.add_change("UPSERT", DNSName, "CNAME")
    result = change.add_value(CNameVal)
    result = changes.commit()
    print result
    return result

AccessKey = sys.argv[1]
SecretKey = sys.argv[2]
HostedZoneName = sys.argv[3]
DomainName = sys.argv[4]
DNSName = sys.argv[5]
AliasName = sys.argv[6]
print len(sys.argv)
if len(sys.argv) > 7:
    RoleName = sys.argv[7]
    SessionName = sys.argv[8]
    tmpkey,tmpsecret,tmptoken = tpaasassumerole(AccessKey,SecretKey,RoleName,"testsession")
    Result1 = tpaasmakealiasdns(tmpkey, tmpsecret,HostedZoneName, DomainName, DNSName,AliasName,SessionToken=tmptoken)
    print Result1
else:
    Result2 = tpaasmakealiasdns(AccessKey,SecretKey,HostedZoneName,DomainName,DNSName,AliasName)
    print Result2

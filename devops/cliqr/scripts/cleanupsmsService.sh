#!/bin/bash

removePuppetConf() {
        sed -i -e "/$2/,+4d" $1
}


SVCNAME=$1
ENVNAME=$2
USERENV=`echo /var/tmp/envs/$3`
TMPDIR="/paas/cliqrtmp"
TEMPLATEDIR="/paas/puppettemplates"
PUPPETDIR="/paas/puppet/tpaas"
PUPPETCONF="/etc/puppet/puppet.conf"
HIERADIR="/var/lib/hiera"
. $USERENV

ENV="${ENVNAME}cliqr${parentJobId}"
PUPPETENV="tpaas${SVCNAME}${ENV}"

# Removing cliqr tmp Directory
rm -rvf $TMPDIR/$SVCNAME/$parentJobId

# Removing Puppet changes
rm -rvf $PUPPETDIR/$SVCNAME/cliqrstacks/$ENV

# Removing Yaml Datasource
rm -rvf $HIERADIR/$PUPPETENV.yaml

# Removing Puppet conf entry
removePuppetConf $PUPPETCONF $PUPPETENV

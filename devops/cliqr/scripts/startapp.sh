#!/bin/bash

replaceLine() {
        lines=$(cat -n $1 | grep "$2" | awk '{print $1}')
        line=${lines[0]}
        if [ "$line" != "" ]; then
                echo "Replacing line $line in $1"
                sed "$line""s/.*/$3/" -i $1
        else
                echo "Insert new line $3 to $1"
                echo "$3" >> $1
        fi
}

. /usr/local/osmosix/etc/userenv

domainName="trimblepaas.com"
HOST_NAME=$1
PUPPETCONF="/etc/puppet/puppet.conf"
SVCNAME=$servicename
ENVNAME=$environment
ENV="${ENVNAME}cliqr${parentJobId}"
PUPPETENV="tpaas${SVCNAME}${ENV}"
BASE_DIR="/opt/remoteFiles/appPackage/devops/cliqr"

# setting Hostname
echo "127.0.0.1 $HOST_NAME.$domainName" > /etc/hosts
#echo "$DeployerIP deployer deployer.trimblepaas.com" >> /etc/hosts

replaceLine /etc/sysconfig/network "HOSTNAME" "HOSTNAME=$HOST_NAME.$domainName"
hostname $HOST_NAME.$domainName
export HOSTNAME=$HOST_NAME.$domainName


#Puppet Client Config
echo "[main]" > /etc/puppet/puppet.conf
echo "logdir=/var/log/puppet" >> /etc/puppet/puppet.conf
echo "vardir=/var/lib/puppet" >> /etc/puppet/puppet.conf
echo "ssldir=/var/lib/puppet/ssl" >> /etc/puppet/puppet.conf
echo "rundir=/var/run/puppet" >> /etc/puppet/puppet.conf
echo "factpath=\$vardir/lib/facter" >> /etc/puppet/puppet.conf
echo "templatedir=\$confdir/templates" >> /etc/puppet/puppet.conf
echo "server=deployer.$domainName" >> /etc/puppet/puppet.conf
echo "waitforcert=60" >> /etc/puppet/puppet.conf
echo "report=false" >> /etc/puppet/puppet.conf
echo "certificate_revocation=false" >> /etc/puppet/puppet.conf
echo "[agent]" >> /etc/puppet/puppet.conf
echo "environment=$PUPPETENV" >> /etc/puppet/puppet.conf

echo "*.$domainName" > /etc/puppet/autosign.conf

rm -rf /var/lib/puppet/ssl/*

if [ -z "$ARTILINK" ]
then
        echo "http://artifactory.trimblepaas.com/artifactory/libs-snapshot-local/com/trimble/service/$SVCNAME/${AppVersion}-SNAPSHOT/" > /etc/artiurl
else
        echo "http://$ARTILINK" > /etc/artiurl
fi

# Puppet Init
puppet agent --enable
puppet agent -vt --debug --certname=$PUPPETENV.$domainName &> /tmp/puppetinit.log


/root/bin/puppet_init.sh $ARTILINK
if [[ "${ENVNAME}" =~ ^pt* ]]
then
bash -x $BASE_DIR/newrelic/newrelic_system_monitor.sh >> /tmp/newrelicmonitor.log
fi

if [[ "${ENVNAME}" =~ ^pt* ||  "${ENVNAME}" =~ ^st* || "${ENVNAME}" =~ ^pr* ]]
then
## Install loging and monitoring agents

bash -x $BASE_DIR/sumologic/install.sh  >> /tmp/sumologic.txt
#sleep 30
bash -x $BASE_DIR/dd-agent/dd_install.sh >> /tmp/ddinstall.log
bash -x $BASE_DIR/dd-agent/dd_addmonitors.sh  >> /tmp/ddaddmonitors.log
bash -x $BASE_DIR/dd-agent/dd_db_app.sh  >> /tmp/ddadddb.log
#bash -x $BASE_DIR/dd-agent/dd_usercreation.sh $cliqrOwner >> /tmp/dduseradd.sh
fi
exit 0

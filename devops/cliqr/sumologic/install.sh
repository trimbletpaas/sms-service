#TYPE=$1

. /usr/local/osmosix/etc/userenv
WGET=`which wget`
ARTURL="http://artifactory.trimblepaas.com/artifactory/thirdparty/SumoCollector/SumoCollector-19.101-10.x86_64.rpm"

BASEDIR="/opt/remoteFiles/appPackage/devops/cliqr/"

HOSTNAME=`hostname`
SERVER_TYPE=`hostname | awk -F"." '{print $1}'`

TYPE="app"
instance=`echo "$servicename-$SERVER_TYPE-$environment"`

cd $BASEDIR/sumologic

${WGET} -q ${ARTURL} --user=tpaas --password='yMGfBCF7n36ujKbc'

sudo yum install -y $BASEDIR/sumologic/SumoCollector-19.101-10.x86_64.rpm

echo "Installation completed"

echo " creating a sumo config file"

replaceToken() {
        sed -i "s,$2,$3,g" $1
}

replaceToken $BASEDIR/sumologic/sources/$TYPE-sourcefile.json %instance% $instance

touch "/etc/sumo.conf"
mkdir -p /etc/sumologic
mv $BASEDIR/sumologic/sources/$TYPE-sourcefile.json /etc/sumologic/

echo " File creation completed"

echo "name=$instance" > /etc/sumo.conf
echo "accessid=suUUVpK4Ybb4vx" >> /etc/sumo.conf
echo "accesskey=xm1q7ZyeFP2niYffEJNGYzlHFZySqivP6Wlek63EMXoF0Gki56NfGM1URgvtAoTw" >> /etc/sumo.conf
echo "sources=/etc/sumologic/$TYPE-sourcefile.json" >> /etc/sumo.conf

echo " Restarting the collector"

sudo /etc/init.d/collector restart

echo " collector installed succesfully "

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema TRIMBLE_SMS_DB
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `TRIMBLE_SMS_DB` ;

-- -----------------------------------------------------
-- Schema TRIMBLE_SMS_DB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `TRIMBLE_SMS_DB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `TRIMBLE_SMS_DB` ;



-- -----------------------------------------------------
-- Table `TRIMBLE_SMS_DB`.`SMS_MESSAGE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TRIMBLE_SMS_DB`.`SMS_MESSAGE` (
  `id` VARCHAR(100) NOT NULL COMMENT '',
  `reference_id` VARCHAR(100) NOT NULL COMMENT '',
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `date_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TRIMBLE_SMS_DB`.`SMS_SHORTCODE_LIST`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TRIMBLE_SMS_DB`.`SMS_SHORTCODE_LIST` (
  `id` VARCHAR(100) NOT NULL COMMENT '',
  `type` ENUM('SH', 'SM') NOT NULL DEFAULT 'SM' COMMENT '',
  `provider` VARCHAR(100) NOT NULL COMMENT '',
  `number` VARCHAR(100) NOT NULL COMMENT '',
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `date_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  PRIMARY KEY (`id`, `type`, `provider`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TRIMBLE_SMS_DB`.`SMS_MESSAGE_ERROR`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TRIMBLE_SMS_DB`.`SMS_MESSAGE_ERROR` (
  `id` VARCHAR(100) NOT NULL COMMENT '',
  `recipient` VARCHAR(15) NOT NULL COMMENT '',
  `error_message` VARCHAR(255) NOT NULL COMMENT '',
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `date_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `date_sent` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TRIMBLE_SMS_DB`.`SMS_USAGE_ARCHIVE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TRIMBLE_SMS_DB`.`SMS_USAGE_ARCHIVE` (
  `appId` VARCHAR(100) NOT NULL COMMENT '',
  `name` VARCHAR(255) NOT NULL COMMENT '',
  `owner` VARCHAR(100) NOT NULL COMMENT '',
  `usage_month` VARCHAR(15) NOT NULL COMMENT '',
  `num_sms_sent` INT(11) NOT NULL DEFAULT 0 COMMENT '',
  `usage_amount` DOUBLE NOT NULL DEFAULT 0 COMMENT '',
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `date_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  PRIMARY KEY (`appId`, `usage_month`)  COMMENT '')
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

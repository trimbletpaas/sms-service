-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema TRIMBLE_SMS_DB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema TRIMBLE_SMS_DB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `TRIMBLE_SMS_DB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `TRIMBLE_SMS_DB` ;


-- -----------------------------------------------------
-- Table `TRIMBLE_SMS_DB`.`SMS_APPLICATION`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TRIMBLE_SMS_DB`.`SMS_APPLICATION` (
  `id` VARCHAR(100) NOT NULL COMMENT '',
  `application_name` VARCHAR(100) NOT NULL COMMENT '',
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `date_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TRIMBLE_SMS_DB`.`SMS_MESSAGE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TRIMBLE_SMS_DB`.`SMS_MESSAGE` (
  `id` VARCHAR(100) NOT NULL COMMENT '',
  `application_id` VARCHAR(100) NOT NULL COMMENT '',
  `provider_reference_id` VARCHAR(100) NOT NULL COMMENT '',
  `provider_name` VARCHAR(100) NOT NULL COMMENT '',
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `date_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `Message_appln_foreign_key_idx` (`application_id` ASC)  COMMENT '',
  CONSTRAINT `Message_appln_foreign_key`
    FOREIGN KEY (`application_id`)
    REFERENCES `TRIMBLE_SMS_DB`.`SMS_APPLICATION` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TRIMBLE_SMS_DB`.`SMS_SHORTCODE_LIST`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TRIMBLE_SMS_DB`.`SMS_SHORTCODE_LIST` (
  `id` VARCHAR(100) NOT NULL COMMENT '',
  `shortcode` VARCHAR(100) NOT NULL COMMENT '',
  `application_id` VARCHAR(100) NOT NULL COMMENT '',
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `date_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `APPLN_KEY_idx` (`application_id` ASC)  COMMENT '',
  CONSTRAINT `APPLN_KEY`
    FOREIGN KEY (`application_id`)
    REFERENCES `TRIMBLE_SMS_DB`.`SMS_APPLICATION` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

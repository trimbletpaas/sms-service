package com.trimble.sms.test;

import static org.junit.Assert.*;

import com.trimble.sms.representations.SMSDetailResponse;
import com.trimble.sms.representations.SendSMSListResponse;
import com.trimble.sms.representations.SendSMSRequest;
import com.trimble.sms.representations.SendSMSResponse;
import com.trimble.sms.services.impl.SmsServiceImpl;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert.*;
import org.junit.*;

import java.io.IOException;
import java.lang.String;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import javax.mail.MessagingException;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.trimble.apiutil.context.JWTContext;
import com.trimble.apiutil.context.JWTContextDetails;
import com.trimble.apiutil.exception.APIException;
import com.trimble.apiutil.reader.JSONPropertyReader;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SMSTest extends JerseyTest {

    private static final Logger logger = LoggerFactory
            .getLogger(SMSTest.class);

    private Gson gson = new Gson();
    ObjectMapper mapper = new ObjectMapper();
    private String configJson = "configuration.json";
    private String environmentName = "bamboo";
    private static String TEST_MESSAGE = "Test Message from TPaaS-Unit Test";
    private static String RECIPIENT1 = "+91-988 444 2633";
    private static String INVALID_RECIPIENTS[] = {"91-988 444 2633","988 444 2633","+91-9884 44 2633", "+91-988 44 4 2633","+91-988 444 26 33"};
    private static String QUEUED_STATUS = "queued";
    private static String DELIVERED_STATUS = "delivered";
    private static String FAILED_STATUS = "FAILED";
    private static String APP_NAME = "SMSJunit";
    private static String EMAIL = "anuradha_mukundan@trimble.com";
    private static String APP_ID ="1";
    private static String ENVIRONMENT_NAME_PROPERTY = "environmentName";
    private static String MESSAGE_ID=" ";
    @Override
    protected Application configure() {
        return new ResourceConfig(SmsServiceImpl.class);
    }

    @Before
    public void setUp() throws Exception {
        loadConfigurations();
        loadJwtDetails();
    }

    private void loadJwtDetails() {
        JWTContextDetails details = new JWTContextDetails();
        details.setApplicationId(APP_ID);
        details.setApplicationName(APP_NAME);
        details.setEndUser(EMAIL);
        details.setEmailaddress(EMAIL);
        JWTContext.set(details);
        // TODO Auto-generated method stub

    }

    @After
    public void tearDown() throws Exception {
        JWTContext.remove();
    }

    public static void main(String[] args) {
        new JUnitCore().run(SMSTest.class);
    }

    /*Loads the environment json*/
    public void loadConfigurations() throws APIException, MessagingException {

        System.setProperty(ENVIRONMENT_NAME_PROPERTY, environmentName);
        new JSONPropertyReader().loadObjectFromJSON(configJson);
    }

    /*Asserts success on sending an SMS to a single recipient */
    @Test
   // @Ignore
    public void case1testSendingSMS() throws APIException{
        SmsServiceImpl impl = new SmsServiceImpl();
        List<String> recipients = new ArrayList<String>();
        recipients.add(RECIPIENT1);
        final Response response = impl.smsPost(createPostRequest(TEST_MESSAGE,recipients,Boolean.FALSE));
        assertNotNull(response);
        logger.info(response.getEntity().toString());
        SendSMSListResponse objectResponse = (SendSMSListResponse) response.getEntity();
        assertNotEquals(objectResponse.getSentSMSList().size(),0);
        assertNotNull(objectResponse.getSentSMSList().get(0).getId());
        assertEquals(objectResponse.getSentSMSList().get(0).getStatus(), QUEUED_STATUS);
        MESSAGE_ID=objectResponse.getSentSMSList().get(0).getId();
    }

    private SendSMSRequest createPostRequest(String message, List<String> recipients, boolean toUseShortcode){
        SendSMSRequest request = new SendSMSRequest();
        request.setMessage(TEST_MESSAGE);
        request.setTo(recipients);
        if(toUseShortcode)
            request.setUseShortCode(Boolean.TRUE);
        return request;
    }
    /*Asserts success on fetching an SMS details given an id */
    @Test
    //@Ignore
    public void case2testGetSMSById() throws APIException{
        case1testSendingSMS();
        String id = MESSAGE_ID;
        SmsServiceImpl impl = new SmsServiceImpl();
        final Response response = impl.smsSmsIdGet(id);
        assertNotNull(response);
        SMSDetailResponse objectResponse = gson.fromJson(response.getEntity().toString(), SMSDetailResponse.class);
        assertNotNull(objectResponse.getId());
        assertEquals(objectResponse.getId(),id);
        assertEquals(objectResponse.getMessage(),TEST_MESSAGE);
    }

    /*Asserts success on sending an SMS to multiple recipients */
    @Test
    //@Ignore
    public void case3testSendingSMSWithMultipleRecipients() throws APIException{
        SmsServiceImpl impl = new SmsServiceImpl();
        List<String> recipients = new ArrayList<String>();
        recipients.add(RECIPIENT1);
        recipients.add(RECIPIENT1);
        final Response response = impl.smsPost(createPostRequest(TEST_MESSAGE,recipients,Boolean.FALSE));
        assertNotNull(response);
        SendSMSListResponse objectResponse = gson.fromJson(response.getEntity().toString(), SendSMSListResponse.class);
        assertNotSame(objectResponse.getSentSMSList().size(),1);
    }

    /*Asserts failure on sending an SMS to multiple invalid recipients */
    @Test
   // @Ignore
    public void case4testSendingSMSWithInvalidRecipient() throws APIException{
        SmsServiceImpl impl = new SmsServiceImpl();
        List<String> recipients = new ArrayList<String>();
        recipients.addAll(Arrays.asList(INVALID_RECIPIENTS));
        final Response response = impl.smsPost(createPostRequest(TEST_MESSAGE,recipients,Boolean.FALSE));
        assertNotNull(response);
        SendSMSListResponse objectResponse = gson.fromJson(response.getEntity().toString(), SendSMSListResponse.class);
        for(SendSMSResponse smsResponse:objectResponse.getSentSMSList()){
            assertEquals(smsResponse.getStatus(),FAILED_STATUS);
        }
    }

    /*Asserts success on sending an SMS to a recipient through shortcode */
    @Test
    //@Ignore
    public void case5testSendingSMSThruShortcode() throws APIException{
        SmsServiceImpl impl = new SmsServiceImpl();
        List<String> recipients = new ArrayList<String>();
        recipients.add(RECIPIENT1);
        final Response response = impl.smsPost(createPostRequest(TEST_MESSAGE,recipients,Boolean.TRUE));
        assertNotNull(response);
        logger.info(response.getEntity().toString());
        SendSMSListResponse objectResponse = (SendSMSListResponse) response.getEntity();
        assertNotNull(objectResponse.getSentSMSList().get(0).getId());
        assertEquals(objectResponse.getSentSMSList().get(0).getStatus(), FAILED_STATUS);
    }


}

package com.trimble.sms.services.impl;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import com.trimble.sms.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trimble.apiutil.context.JWTContext;
import com.trimble.apiutil.exception.APIException;
import com.trimble.apiutil.model.APIGenericResponse;
import com.trimble.sms.dao.representations.Error;
import com.trimble.sms.dao.representations.Number;
import com.trimble.sms.representations.ListSMSUsageDetailResponse;
import com.trimble.sms.representations.SMSDetailResponse;
import com.trimble.sms.representations.SMSListResponse;
import com.trimble.sms.representations.SMSMediaListResponse;
import com.trimble.sms.representations.SMSMediaResponse;
import com.trimble.sms.representations.SMSNumberListResponse;
import com.trimble.sms.representations.SMSNumberResponse;
import com.trimble.sms.representations.SMSUsageDetailResponse;
import com.trimble.sms.representations.SMSUsageResponse;
import com.trimble.sms.representations.SendSMSListResponse;
import com.trimble.sms.representations.SendSMSRequest;
import com.trimble.sms.representations.SendNumberRequest;
import com.trimble.sms.representations.SendNumberResponse;
import com.trimble.sms.services.SmsService;
import com.twilio.sdk.resource.instance.Media;
import com.twilio.sdk.resource.instance.Message;
import com.twilio.sdk.resource.list.MediaList;
import com.twilio.sdk.resource.list.MessageList;

public class SmsServiceImpl implements SmsService {

	private static final Logger logger = LoggerFactory.getLogger(SmsServiceImpl.class);
	
  
    @Override
    public Response smsGet(String from,String to,String dateSent,int maxNumberSMS) throws APIException {
    	SMSListResponse response =null;
 		logger.info("Into smsGet");
		logger.info("GET all called with from {}, to {}, dateSent {}, maxNumberSMS {} from application {}", from, to, dateSent, maxNumberSMS, JWTContext.get().getApplicationName());
 		try{
			if (from!=null && !new ValidationUtil().validatePhone(from)) {
				logger.error("Invalid from number {}", from);
				throw new APIException(MessageFormat.format("Invalid from number {0}", from),Constants.HTTP_STATUS_BAD_REQUEST);
			}
			if(to!=null && !new ValidationUtil().validatePhone(to)) {
				logger.error("Invalid to number {}", to);
				throw new APIException(MessageFormat.format("Invalid to number {0}", to),Constants.HTTP_STATUS_BAD_REQUEST);
			}
            if(dateSent!=null && !new ValidationUtil().isValidDateFormat(dateSent)){
                logger.error("Invalid date format specified {}. Date should follow the following format: {}", dateSent,"yyyy-MM-dd");
                throw new APIException(MessageFormat.format("Invalid date format specified {0}. Date should follow the following format: {1}", dateSent,"yyyy-MM-dd"),Constants.HTTP_STATUS_BAD_REQUEST);
            }
			if(maxNumberSMS<=0)
				maxNumberSMS =Constants.DEFAULT_MAX_RETRIEVE_COUNT;
			if(!new ValidationUtil().validateMaxRetrieveCount(maxNumberSMS)) {
				logger.error("Invalid maxNumberSMS {}", maxNumberSMS);
				throw new APIException(MessageFormat.format("Single GET call can fetch only upto {0} messages", ValidationUtil.getMaxretrieveCount()),Constants.HTTP_STATUS_BAD_REQUEST);
			}

 		MessageList readMessages = new TwilioAPIUtil().readMessageList(from, to,dateSent);
    	Map<String,com.twilio.sdk.resource.instance.Message> tpaasMsgIdMapping = new DatabaseUtil().readMessageDetails(readMessages,false, dateSent);
        response = new RequestResponseUtil().formGETResponse(tpaasMsgIdMapping,maxNumberSMS);
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
		return Response.ok().entity(response).build();
	}
  
	@Override
    public Response smsPost(SendSMSRequest sendSMSRequest) throws APIException {
		SendSMSListResponse sentMessages =null;
 		logger.info("Into smsPost");
		logger.info("SMS send Request from application {} : \n {}", JWTContext.get().getApplicationName(), sendSMSRequest.toString());
 		try{
			//validate recipients count
			if(sendSMSRequest.getTo().isEmpty())
				throw new APIException("No Valid Recipients to send the message.", Constants.HTTP_STATUS_BAD_REQUEST);
			//validate message max length
			if(!new ValidationUtil().validateMessageLength(sendSMSRequest.getMessage()))
			    throw new APIException(MessageFormat.format("Message cannot exceed more than {0} characters", ValidationUtil.getMaxMessageLength()), Constants.HTTP_STATUS_BAD_REQUEST);
			//send message through Twilio and persist referenceId
			sentMessages = new TwilioAPIUtil().sendMessage(sendSMSRequest);
    	//new DatabaseUtil().writeSentMessageDetails(sentMessages);
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
		return Response.ok().entity(sentMessages).build();
		
	}

	@Override
    public Response smsSmsIdGet(String smsId) throws APIException {
		SMSDetailResponse response = null;
 		logger.info("Into smsSmsIdGet");
		logger.info(" SMS GET call for id {} from application {}",smsId, JWTContext.get().getApplicationName());
 		try{
 		String referenceId= new DatabaseUtil().fetchReferenceId(smsId);
 		if(referenceId==null||referenceId.isEmpty())
 			throw new APIException("No record found with the ID inputted.", Constants.HTTP_STATUS_NOT_FOUND);
 		Message readMessage = new TwilioAPIUtil().getMessage(referenceId);
 		if(readMessage==null){
 			Error err = new DatabaseUtil().readErrorDetails(referenceId);
 			response = new RequestResponseUtil().formGetByIdErrResponse(referenceId,smsId,err);
 		}
 		else	
 			response = new RequestResponseUtil().formGetByIdResponse(referenceId, smsId, readMessage);
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
    	return Response.ok().entity(response).build();
	}
  
    /*private SMSDetailResponse generateGetByIdResponse(String smsId) {
    	SMSDetailResponse res = new SMSDetailResponse();
 		res.setErrorCode(null);
 		res.setErrorMessage(null);
 		res.setFrom("<senderNumber>");
 		res.setId(smsId);
 		res.setStatus("Delivered");
 		res.setTo("<recipientNumber>");
 		res.setDateCreated(new java.util.Date().toString());
 		res.setDateSent(new java.util.Date().toString());
 		res.setDateUpdated(new java.util.Date().toString());
 		res.setMessage("Welcome To TPaaS SMS Service");
 		res.setNumberOfmedia(2);
		return res;
	}*/

	@Override
    public Response smsSmsIdMediaGet(String smsId) throws APIException {
		SMSMediaListResponse response = null;
 		logger.info("Into smsSmsIdMediaGet");
		logger.info("SMS Media GET call for id {} from application {}", smsId, JWTContext.get().getApplicationName());
 		try{
 		String referenceId= new DatabaseUtil().fetchReferenceId(smsId);
			if(referenceId==null || referenceId.isEmpty())
				throw new APIException("Message with the ID inputted does not exist.",Constants.HTTP_STATUS_NOT_FOUND);
 		MediaList readMedia = new TwilioAPIUtil().getMediasOfAMessage(referenceId);
			if(readMedia==null || !readMedia.iterator().hasNext()){
				throw new APIException("There are no media asscociated with the given id.", Constants.HTTP_STATUS_NOT_FOUND);
			}
    	response = new RequestResponseUtil().formMediaListResponse(smsId, readMedia);
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
    	return Response.ok().entity(response).build();
	}
  
   /* private SMSMediaListResponse generateMediaListResponse(String smsId) {
    	SMSMediaListResponse mediaList = new SMSMediaListResponse();
 		List<SMSMediaResponse> list = new ArrayList<SMSMediaResponse>();
 		SMSMediaResponse resp = new SMSMediaResponse();
 		resp.setId("Media"+UUID.randomUUID().toString());
 		resp.setContentType("image/jpeg");
 		resp.setDateCreated(new java.util.Date().toString());
 		resp.setDateUpdated(new java.util.Date().toString());
 		resp.setMediaURL("http://7-themes.com/data_images/out/63/6986083-waterfall-images.jpg");
 		resp.setSmsId(smsId);
 		list.add(resp);
 		resp = new SMSMediaResponse();
 		resp.setId("Media"+UUID.randomUUID().toString());
 		resp.setContentType("image/jpeg");
 		resp.setDateCreated(new java.util.Date().toString());
 		resp.setDateUpdated(new java.util.Date().toString());
 		resp.setMediaURL("http://7-themes.com/data_images/out/38/6900927-cool-wallpapers-hd.jpg");
 		resp.setSmsId(smsId);
 		list.add(resp);
 		mediaList.setMediaList(list);		
		return mediaList;
	}*/

	@Override
    public Response smsSmsIdMediaMediaIdGet(String smsId,String mediaId) throws APIException {
		SMSMediaResponse response = null;
 		logger.info("Into smsSmsIdMediaMediaIdGet");
		logger.info("SMS Media GET request for id {} and media id {} from application {}", smsId, mediaId, JWTContext.get().getApplicationName());
 		try{
 		String referenceId= new DatabaseUtil().fetchReferenceId(smsId);
			if(referenceId==null || referenceId.isEmpty())
				throw new APIException("Message with the ID inputted does not exist.",Constants.HTTP_STATUS_NOT_FOUND);
 		Media readMedia = new TwilioAPIUtil().getAMediaMessage(referenceId,mediaId);
			if(readMedia==null){
				throw new APIException("There are no media asscociated with the given media id.", Constants.HTTP_STATUS_NOT_FOUND);
			}
    	response = new RequestResponseUtil().formMediaResponse(smsId, readMedia);
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
    	return Response.ok().entity(response).build();
	}

	public Response smsNumberGet() throws APIException {
		SMSNumberListResponse response =  null;
		logger.info("Into smsNumberGet");
		logger.info("SMS Admin Numbers GET");
		try{
		List<Number> codes = new DatabaseUtil().readNumbers();
    	response = new RequestResponseUtil().formNumbersGETResponse(codes);
	    }
		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
			ae.printStackTrace();
			throw ae;
		}
		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
			e.printStackTrace();
			throw new APIException(e.getMessage());
		}
		return Response.ok().entity(response).build();
	}

	public Response smsNumberPost(SendNumberRequest sendshortcodeRequest) throws APIException {
		SendNumberResponse response =null;
 		logger.info("Into smsNumberPost");
		logger.info("SMS Admin Numbers POST request: \n {}", sendshortcodeRequest.toString());
 		try{
 			if((sendshortcodeRequest.getNumber()==null || sendshortcodeRequest.getNumber().isEmpty())&&(sendshortcodeRequest.getShortcode()==null||sendshortcodeRequest.getShortcode().isEmpty()))
				throw new APIException("Number/Shortcode is missing. Provide atleast one of them", Constants.HTTP_STATUS_BAD_REQUEST);
			if (sendshortcodeRequest.getNumber()!=null && !new ValidationUtil().validatePhone(sendshortcodeRequest.getNumber())) {
				logger.error("Invalid number {}", sendshortcodeRequest.getNumber());
				throw new APIException(MessageFormat.format("Invalid number {0}", sendshortcodeRequest.getNumber()),Constants.HTTP_STATUS_BAD_REQUEST);
			}
 			if(sendshortcodeRequest.getAppId()==null || sendshortcodeRequest.getAppId().isEmpty()) 
 				sendshortcodeRequest.setAppId(JWTContext.get().getApplicationId());
 			new DatabaseUtil().writeNumberDetails(sendshortcodeRequest);
 			response = new RequestResponseUtil().formNumberPOSTResponse(sendshortcodeRequest);
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
		return Response.ok().entity(response).build();
	}

	public Response smsNumberPut(String appId, SendNumberRequest sendshortcodeRequest) throws APIException{
 		logger.info("Into smsNumberPut");
		logger.info("SMS Admin number update request: \n {}", sendshortcodeRequest);
 		try{
			List<Number> code = new DatabaseUtil().readNumberById(appId);
			if(code==null || code.isEmpty()){
				logger.error("No numbers associated for the given app id {}", appId);
				throw new APIException(MessageFormat.format("No numbers associated for the given app id {0}",appId),Constants.HTTP_STATUS_NOT_FOUND);
			}
			if (sendshortcodeRequest.getNumber()!=null && !new ValidationUtil().validatePhone(sendshortcodeRequest.getNumber())) {
				logger.error("Invalid number {}", sendshortcodeRequest.getNumber());
				throw new APIException(MessageFormat.format("Invalid number {0}", sendshortcodeRequest.getNumber()),Constants.HTTP_STATUS_BAD_REQUEST);
			}
			sendshortcodeRequest.setAppId(appId);
 			new DatabaseUtil().updateNumberDetails(sendshortcodeRequest);
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
 		APIGenericResponse response=new APIGenericResponse();
 		response.setType("INFO");
 		response.setMessage("UPDATED");
		return Response.ok().entity(response).build();
	}

	public Response smsNumberGETByID(String appId) throws APIException {
		SMSNumberResponse response =  null;
		logger.info("Into smsNumberGETByID");
		logger.info("sms Admin number GET request for app id {}", appId);
		try{
		List<Number> code = new DatabaseUtil().readNumberById(appId);
			if(code==null || code.isEmpty()){
				logger.error("No numbers associated for the given app id {}", appId);
				throw new APIException(MessageFormat.format("No numbers associated for the given app id {0}",appId),Constants.HTTP_STATUS_NOT_FOUND);
			}

    	response = new RequestResponseUtil().formNumberGETByIdResponse(code);
	    }
		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
			ae.printStackTrace();
			throw ae;
		}
		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
			e.printStackTrace();
			throw new APIException(e.getMessage());
		}
		return Response.ok().entity(response).build();
	}

	public Response smsUsageGET() throws APIException {
		SMSUsageDetailResponse response = null;
 		logger.info("Into smsSmsIdMediaMediaIdGet");
		logger.info("SMS Admin Usage GET for all Applications");
 		try{
 			response = new TwilioAPIUtil().getUsage();
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
    	return Response.ok().entity(response).build();
	}

	public Response smsUsageGETToday() throws APIException {
		SMSUsageResponse response = null;
 		logger.info("Into smsSmsIdMediaMediaIdGet");
 		try{
 			response = new TwilioAPIUtil().getUsageToday();
 		}
 		catch(APIException ae){
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
    	return Response.ok().entity(response).build();
	}

	public Response smsUsageGETyesterday() throws APIException {
		SMSUsageResponse response = null;
 		logger.info("Into smsSmsIdMediaMediaIdGet");
 		try{
 			response = new TwilioAPIUtil().getUsageYesterday();
 		}
 		catch(APIException ae){
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
    	return Response.ok().entity(response).build();
	}

	public Response smsUsageGETThisMonth() throws APIException {
		SMSUsageResponse response = null;
 		logger.info("Into smsUsageGETThisMonth");
		logger.info("SMS Usage request for current month for application: {} ", JWTContext.get().getApplicationName());
 		try{
 			response = new TwilioAPIUtil().getUsageThisMonth();
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
    	return Response.ok().entity(response).build();
	}

	public Response smsUsageGETLastMonth() throws APIException {
		SMSUsageResponse response = null;
 		logger.info("Into smsUsageGETLastMonth");
		logger.info("SMS Usage request for last month for application: {} ", JWTContext.get().getApplicationName());
 		try{
 			response = new TwilioAPIUtil().getUsageLastMonth();
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
    	return Response.ok().entity(response).build();
	}

	public Response smsUsageAllGET() throws APIException {
		ListSMSUsageDetailResponse response = null;
 		logger.info("Into smsUsageAllGET");
		logger.info("SMS get complete usage of the application {}", JWTContext.get().getApplicationName());
 		try{
 			response = new TwilioAPIUtil().getAllUsage();
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
    	return Response.ok().entity(response).build();
	}

	public Response smsGetAll(String from, String to, String dateSent, int maxNumberSMS) throws APIException {
		SMSListResponse response =null;
 		logger.info("Into smsGetAll for all applications");
		logger.info("GET All request for from {}, to {}, dateSent {}, maxNumberSMS {}", from, to, dateSent, maxNumberSMS);
 		try{
			if (from!=null && !new ValidationUtil().validatePhone(from)) {
				logger.error("Invalid from number {}", from);
				throw new APIException(MessageFormat.format("Invalid from number {0}", from),Constants.HTTP_STATUS_BAD_REQUEST);
			}
			if(to!=null && !new ValidationUtil().validatePhone(to)) {
				logger.error("Invalid to number {}", to);
				throw new APIException(MessageFormat.format("Invalid to number {0}", to),Constants.HTTP_STATUS_BAD_REQUEST);
			}
            if(dateSent!=null && !new ValidationUtil().isValidDateFormat(dateSent)){
                logger.error("Invalid date format specified {}. Date should follow the following format: {}", dateSent,Constants.DATE_FORMAT);
                throw new APIException(MessageFormat.format("Invalid date format specified {0}. Date should follow the following format: {1}", dateSent,Constants.DATE_FORMAT),Constants.HTTP_STATUS_BAD_REQUEST);
            }
			if(maxNumberSMS<=0)
				maxNumberSMS =Constants.DEFAULT_MAX_RETRIEVE_COUNT;
			if(!new ValidationUtil().validateMaxRetrieveCount(maxNumberSMS)) {
				logger.error("Invalid maxNumberSMS {}", maxNumberSMS);
				throw new APIException(MessageFormat.format("Single GET call can fetch only upto {0} messages", ValidationUtil.getMaxretrieveCount()),Constants.HTTP_STATUS_BAD_REQUEST);
			}
 		MessageList readMessages = new TwilioAPIUtil().readMessageList(from, to,dateSent);
    	Map<String,com.twilio.sdk.resource.instance.Message> tpaasMsgIdMapping = new DatabaseUtil().readMessageDetails(readMessages,true, dateSent);
        response = new RequestResponseUtil().formGETResponse(tpaasMsgIdMapping,maxNumberSMS);
 		}
 		catch(APIException ae){
			logger.error("Exception  Occurred: {}", ae.getMessage());
 			ae.printStackTrace();
 			throw ae;
 		}
 		catch(Exception e){
			logger.error("Exception  Occurred: {}", e.getMessage());
 			e.printStackTrace();
 			throw new APIException(e.getMessage());
 		}
		return Response.ok().entity(response).build();
	}

    public Response smsNumberDELETEByID(String appId) throws APIException {
        logger.info("Into smsNumberDELETEByID");
        logger.info("SMS Admin number DELETE request: \n {}", appId);
        try{
            List<Number> code = new DatabaseUtil().readNumberById(appId);
            if(code==null || code.isEmpty()){
                logger.error("No numbers associated for the given app id {}", appId);
                throw new APIException(MessageFormat.format("No numbers associated for the given app id {0}",appId),Constants.HTTP_STATUS_NOT_FOUND);
            }
            new DatabaseUtil().DeleteNumberDetails(appId);
        } catch(APIException ae){
            logger.error("Exception  Occurred: {}", ae.getMessage());
            ae.printStackTrace();
            throw ae;
        }
        catch(Exception e){
            logger.error("Exception  Occurred: {}", e.getMessage());
            e.printStackTrace();
            throw new APIException(e.getMessage());
        }
        return Response.noContent().build();
    }

	/*private SMSMediaResponse generateMediaResponse(String smsId, String mediaId) {
		SMSMediaResponse resp = new SMSMediaResponse();
 		resp.setId(mediaId);
 		resp.setContentType("image/jpeg");
 		resp.setDateCreated(new java.util.Date().toString());
 		resp.setDateUpdated(new java.util.Date().toString());
 		resp.setMediaURL("http://7-themes.com/data_images/out/63/6986083-waterfall-images.jpg");
 		resp.setSmsId(smsId);
		return resp;
	}*/
  
}

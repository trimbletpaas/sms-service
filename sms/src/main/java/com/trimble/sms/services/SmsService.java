package com.trimble.sms.services;

import com.trimble.sms.representations.*;
import com.trimble.apiutil.exception.APIException;


import java.util.List;
import javax.ws.rs.core.Response;

public interface SmsService {
  
      public Response smsGet(String from,String to,String dateSent,int maxNumberSMS) throws APIException;
  
      public Response smsPost(SendSMSRequest sendSMSRequest) throws APIException;
  
      public Response smsSmsIdGet(String smsId) throws APIException;
  
      public Response smsSmsIdMediaGet(String smsId) throws APIException;
  
      public Response smsSmsIdMediaMediaIdGet(String smsId,String mediaId) throws APIException;
  
}

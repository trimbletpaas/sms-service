package com.trimble.sms.resources;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.trimble.apiutil.exception.APIException;
import com.trimble.apiutil.model.APIGenericResponse;
import com.trimble.sms.representations.ListSMSUsageDetailResponse;
import com.trimble.sms.representations.SMSListResponse;
import com.trimble.sms.representations.SMSNumberListResponse;
import com.trimble.sms.representations.SMSNumberResponse;
import com.trimble.sms.representations.SendNumberRequest;
import com.trimble.sms.services.impl.SmsServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.io.IOException;

@Path("/admin")
@Api(value = "/admin", description = "the admin API")
@Produces({ "application/json" })
public class SmsAdminResource {
	  @GET
	    @Path("/sms")
	    
	    @ApiOperation(value = "", notes = "", response = SMSListResponse.class)
	    @ApiResponses(value = { 
	        @ApiResponse(code = 200, message = "Get all the messages associated with an application"),
	        @ApiResponse(code = 0, message = "Unexpected error")})
	    public Response smsGetAll(@ApiParam(value = "Filter based on sender") @QueryParam("from") String from,
	    							@ApiParam(value = "Filter based on recipient") @QueryParam("to") String to,
	    							@ApiParam(value = "Filter based on sender") @QueryParam("date_sent") String dateSent,
	    							@ApiParam(value = "specify the number of messages to retrieve") @QueryParam("maxNumberSMS") int maxNumberSMS) throws APIException {
	    	return new SmsServiceImpl().smsGetAll(from,to,dateSent,maxNumberSMS);
	    }
	  
	  @GET
	    @Path("/usage")
	    
	    @ApiOperation(value = "", notes = "", response = ListSMSUsageDetailResponse.class)
	    @ApiResponses(value = { 
	        @ApiResponse(code = 200, message = "Get all the media details of a particular SMS"),
	        @ApiResponse(code = 0, message = "Unexpected error")})
	    public Response smsUsageAllGET() throws APIException {
	    	return new SmsServiceImpl().smsUsageAllGET();
	    }
	  
	    @GET
	    @Path("/numbers")
	    
	    @ApiOperation(value = "", notes = "", response = SMSNumberListResponse.class)
	    @ApiResponses(value = { 
	        @ApiResponse(code = 200, message = "Get all the media details of a particular SMS"),
	        @ApiResponse(code = 0, message = "Unexpected error")})
	    public Response smsNumberGet() throws APIException {
	    	return new SmsServiceImpl().smsNumberGet();
	    }
	    
	    @POST
	    @Path("/numbers")
	    
	    @ApiOperation(value = "", notes = "", response = SMSNumberResponse.class)
	    @ApiResponses(value = { 
	        @ApiResponse(code = 200, message = "Get all the media details of a particular SMS"),
	        @ApiResponse(code = 0, message = "Unexpected error")})
	    public Response smsNumberPost(@ApiParam(value = "Shortcode for an application" ,required=true ) @Valid SendNumberRequest sendNumberRequest) throws APIException {
	    	return new SmsServiceImpl().smsNumberPost(sendNumberRequest);
	    }
	    
	    @PUT
	    @Path("/numbers/{appId}")
	    
	    @ApiOperation(value = "", notes = "", response = APIGenericResponse.class)
	    @ApiResponses(value = { 
	        @ApiResponse(code = 200, message = "Get all the media details of a particular SMS"),
	        @ApiResponse(code = 0, message = "Unexpected error")})
	    public Response smsNumberPut(@ApiParam(value = "Enter message ID",required=true ) @PathParam("appId") String appId,@ApiParam(value = "Shortcode for an application" ,required=true ) @Valid SendNumberRequest sendNumberRequest) throws APIException {
	    	return new SmsServiceImpl().smsNumberPut(appId, sendNumberRequest);
	    }
	    
	    @GET
	    @Path("/numbers/{appId}")
	    
	    @ApiOperation(value = "", notes = "", response = SMSNumberResponse.class)
	    @ApiResponses(value = { 
	        @ApiResponse(code = 200, message = "Get number details of a particular application"),
	        @ApiResponse(code = 0, message = "Unexpected error")})
	    public Response smsNumberGETByID(@ApiParam(value = "Enter Number ID",required=true ) @PathParam("appId") String appId) throws APIException {
	    	return new SmsServiceImpl().smsNumberGETByID(appId);
	    }

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/numbers/{appId}")
	@ApiOperation(value = "", notes = "")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Returns delete status"),
			@ApiResponse(code = 400, message = "Bad Request. JWT context does not exists"),
			@ApiResponse(code = 404, message = "Not Found. Requested Application does not exist") })
	public Response smsNumberDELETEByID(
			@ApiParam(value = "App Id", required = true) @PathParam("appId") String appId)
			throws APIException {
		return new SmsServiceImpl()
				.smsNumberDELETEByID(appId);
	}
}

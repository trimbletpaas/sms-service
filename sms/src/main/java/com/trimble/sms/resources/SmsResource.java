package com.trimble.sms.resources;

import com.trimble.sms.representations.*;
import com.trimble.sms.services.SmsService;
import com.trimble.sms.services.impl.SmsServiceImpl;
import com.trimble.apiutil.exception.APIException;
import com.trimble.apiutil.model.APIGenericResponse;
import com.trimble.apiutil.context.JWTContextDetails;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.*;
import javax.validation.Valid;

import java.util.List;
import io.swagger.annotations.*;

@Path("/sms")
@Api(value = "/sms", description = "the sms API")
@Produces({ "application/json" })
public class SmsResource  {
	
	@SuppressWarnings("unchecked")
	@Context JWTContextDetails jwtContextDetails; //To enable building jwt context, uncomment this line.
		
    
    @GET
    
    @ApiOperation(value = "", notes = "", response = SMSListResponse.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Get all the messages associated with an application"),
        @ApiResponse(code = 0, message = "Unexpected error")})
    public Response smsGet(@ApiParam(value = "Filter based on sender") @QueryParam("from") String from,
    							@ApiParam(value = "Filter based on recipient") @QueryParam("to") String to,
    							@ApiParam(value = "Filter based on sent date") @QueryParam("date_sent") String dateSent,
    							@ApiParam(value = "specify the number of messages to retrieve") @QueryParam("maxNumberSMS") int maxNumberSMS) throws APIException {
    	return new SmsServiceImpl().smsGet(from,to,dateSent,maxNumberSMS);
    }
    
    @POST
    
    
    @ApiOperation(value = "", notes = "", response = SendSMSListResponse.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Returns the MessageID and Status"),
        @ApiResponse(code = 0, message = "Unexpected error")})
    public Response smsPost(@ApiParam(value = "Request to send an SMS on behalf of an application" ,required=true ) @Valid SendSMSRequest sendSMSRequest) throws APIException {
    	return new SmsServiceImpl().smsPost(sendSMSRequest);
    }
    
    @GET
    @Path("/{smsId}")
    
    @ApiOperation(value = "", notes = "", response = SMSDetailResponse.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Get the details of a particular SMS"),
        @ApiResponse(code = 0, message = "Unexpected error")})
    public Response smsSmsIdGet(@ApiParam(value = "Enter message ID",required=true ) @PathParam("smsId") String smsId) throws APIException {
    	return new SmsServiceImpl().smsSmsIdGet(smsId);
    }
    
    @GET
    @Path("/{smsId}/media")
    
    @ApiOperation(value = "", notes = "", response = SMSMediaListResponse.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Get all the media details of a particular SMS"),
        @ApiResponse(code = 0, message = "Unexpected error")})
    public Response smsSmsIdMediaGet(@ApiParam(value = "Enter message ID",required=true ) @PathParam("smsId") String smsId) throws APIException {
    	return new SmsServiceImpl().smsSmsIdMediaGet(smsId);
    }
    
    @GET
    @Path("/{smsId}/media/{mediaId}")
    
    @ApiOperation(value = "", notes = "", response = SMSMediaResponse.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Get the requested  media details of a particular SMS"),
        @ApiResponse(code = 0, message = "Unexpected error")})
    public Response smsSmsIdMediaMediaIdGet(@ApiParam(value = "Enter message ID",required=true ) @PathParam("smsId") String smsId,
    							@ApiParam(value = "Enter media ID",required=true ) @PathParam("mediaId") String mediaId) throws APIException {
    	return new SmsServiceImpl().smsSmsIdMediaMediaIdGet(smsId,mediaId);
    }
    
   
    
    @GET
    @Path("/usage")
    
    @ApiOperation(value = "", notes = "", response = SMSUsageDetailResponse.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Get all the media details of a particular SMS"),
        @ApiResponse(code = 0, message = "Unexpected error")})
    public Response smsUsageGET() throws APIException {
    	return new SmsServiceImpl().smsUsageGET();
    }
   
    
   /* @GET
    @Path("/usage/today")
    
    @ApiOperation(value = "", notes = "", response = SMSShortcodeResponse.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Get all the media details of a particular SMS"),
        @ApiResponse(code = 0, message = "Unexpected error")})
    public Response smsUsageGETToday() throws APIException {
    	return new SmsServiceImpl().smsUsageGETToday();
    }
    
    @GET
    @Path("/usage/yesterday")
    
    @ApiOperation(value = "", notes = "", response = SMSShortcodeResponse.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Get all the media details of a particular SMS"),
        @ApiResponse(code = 0, message = "Unexpected error")})
    public Response smsUsageGETYesterday() throws APIException {
    	return new SmsServiceImpl().smsUsageGETyesterday();
    }
    */
    @GET
    @Path("/usage/thisMonth")
    
    @ApiOperation(value = "", notes = "", response = SMSUsageResponse.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Get all the media details of a particular SMS"),
        @ApiResponse(code = 0, message = "Unexpected error")})
    public Response smsUsageGETThisMonth() throws APIException {
    	return new SmsServiceImpl().smsUsageGETThisMonth();
    }
    
    @GET
    @Path("/usage/lastMonth")
    
    @ApiOperation(value = "", notes = "", response = SMSUsageResponse.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Get all the media details of a particular SMS"),
        @ApiResponse(code = 0, message = "Unexpected error")})
    public Response smsUsageGETLastMonth() throws APIException {
    	return new SmsServiceImpl().smsUsageGETLastMonth();
    }
}


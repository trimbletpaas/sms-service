package com.trimble.sms.util;

/**
 * Created by amukunda on 30/03/16.
 */
public class Constants {
    public static final int HTTP_STATUS_BAD_REQUEST = 400;
    public static final int HTTP_STATUS_CONFLICT = 409;
    public static final int HTTP_STATUS_SERVER_ERROR = 500;
    public static final int HTTP_STATUS_NOT_FOUND = 404;
    public static final int HTTP_STATUS_UNAUTHORIZED = 401;
    public static final int HTTP_STATUS_FORBIDDEN = 403;

    public static final String CONFIG_FILE_NAME_PROPERTY = "config.file";

    public static final String PHONE_REGEX_PROPERTY = "phone.format";
    public static final String DEFAULT_PHONE_REGEX = "^\\+[ (]?\\d{1,3}[)]?[ ]?[-]?[ ]?\\d{3}[ ]?[-]?[ ]?\\d{3}[ ]?[-]?[ ]?\\d{4}$";
    public static final String MESSAGE_LENGTH_PROPERTY = "message.max.length";
    public static final int DEFAULT_MESSAGE_LENGTH = 255;

    public static final String MAX_RETRIEVE_COUNT_PROPERTY= "retrieve.max.messages";
    public static final int DEFAULT_MAX_RETRIEVE_COUNT = 10;
    public static final String TWILIO_PROVIDER="TWILIO";
    public static final String DATE_FORMAT ="yyyy-MM-dd";
}

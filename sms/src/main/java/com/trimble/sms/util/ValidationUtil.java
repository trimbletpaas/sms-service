package com.trimble.sms.util;

import com.trimble.apiutil.model.JSONProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by amukunda on 30/03/16.
 */
public class ValidationUtil {

    private static final Logger logger = LoggerFactory
            .getLogger(ValidationUtil.class);

    public static int getMaxretrieveCount() {
        return (JSONProperty.commonProperties
                .getProperty(Constants.MAX_RETRIEVE_COUNT_PROPERTY) == null) ? Constants.DEFAULT_MAX_RETRIEVE_COUNT
                : Integer.parseInt(JSONProperty.commonProperties
                .getProperty(Constants.MAX_RETRIEVE_COUNT_PROPERTY));
    }

    public boolean validatePhone(String recipient) {
        boolean isValid = true;
        Pattern pattern = Pattern
                .compile(getEmailRegex());
        Matcher matcher = pattern.matcher(recipient);
        if (!matcher.matches()) {
            logger.info("Invalid Phone Number: "+recipient);
            isValid = false;
        }
        System.out.println(isValid);
        return isValid;
    }

    public boolean validateMessageLength(String message) {
        boolean isValid = true;
        if(message.length()>=getMaxMessageLength())
            isValid=false;
        return isValid;
    }

    public static int getMaxMessageLength(){
        int maxMessageLength = JSONProperty.commonProperties
                .getProperty(Constants.MESSAGE_LENGTH_PROPERTY) == null? Constants.DEFAULT_MESSAGE_LENGTH: Integer.parseInt(JSONProperty.commonProperties.getProperty(Constants.MESSAGE_LENGTH_PROPERTY));
        return maxMessageLength;
    }

    public static String getEmailRegex() {
        return (JSONProperty.commonProperties
                .getProperty(Constants.PHONE_REGEX_PROPERTY) == null) ? Constants.DEFAULT_PHONE_REGEX
                : JSONProperty.commonProperties
                .getProperty(Constants.PHONE_REGEX_PROPERTY);
    }

    public boolean validateMaxRetrieveCount(Integer maxNumberSMS) {
        return (maxNumberSMS>getMaxretrieveCount())?false:true;
    }

    public Boolean isValidDateFormat(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
        formatter.setLenient(true);
        try {
            formatter.parse(date);
        } catch (ParseException e) {
            logger.error("Invalid date format specified {}. Date should follow the following format: {}", date,Constants.DATE_FORMAT);
            return false;
        }
        return true;
    }
}

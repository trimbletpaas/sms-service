package com.trimble.sms.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.hibernate.HibernateException;

import com.trimble.apiutil.context.JWTContext;
import com.trimble.apiutil.exception.APIException;
import com.trimble.sms.dao.representations.Error;
import com.trimble.sms.dao.representations.Number;
import com.trimble.sms.representations.SMSDetailResponse;
import com.trimble.sms.representations.SMSDetailResponse.TypeEnum;
import com.trimble.sms.representations.SMSListResponse;
import com.trimble.sms.representations.SMSMediaListResponse;
import com.trimble.sms.representations.SMSMediaResponse;
import com.trimble.sms.representations.SMSNumberListResponse;
import com.trimble.sms.representations.SMSNumberResponse;
import com.trimble.sms.representations.SendSMSListResponse;
import com.trimble.sms.representations.SendSMSResponse;
import com.trimble.sms.representations.SendNumberRequest;
import com.trimble.sms.representations.SendNumberResponse;
import com.twilio.sdk.resource.instance.Media;
import com.twilio.sdk.resource.instance.Message;
import com.twilio.sdk.resource.list.MediaList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestResponseUtil {

	private static final Logger logger = LoggerFactory.getLogger(RequestResponseUtil.class);

	public SendSMSListResponse formPOSTResponse(HashMap<String, Message> tpaasMsgIdMapping) {
		SendSMSListResponse listResponse = new SendSMSListResponse();
		for(String id:tpaasMsgIdMapping.keySet()){
			SendSMSResponse response = new SendSMSResponse();
			Message msg = tpaasMsgIdMapping.get(id);
			response.setId(id);
			response.setTo(msg.getTo());
			response.setStatus(msg.getStatus());
			response.setErrorMessage(msg.getErrorMessage());
			try{
			if(msg.getErrorCode()!=null)
				response.setErrorCode(msg.getErrorCode());
			}
			catch(IllegalArgumentException ie){
				response.setErrorCode(null);
			}
			listResponse.getSentSMSList().add(response);
		}
		return listResponse;
	}

	public SMSDetailResponse formGetByIdResponse(String tpaasId, String smsId, Message msg) {
		SMSDetailResponse response = new SMSDetailResponse();
		response.setId(smsId);
		if(tpaasId.contains("-IN-"))
			response.setType(TypeEnum.Incoming);
		else
			response.setType(TypeEnum.Outgoing);
		if(tpaasId.contains("-SH-"))
			response.setSentUsing("Shortcode");
		response.setTo(msg.getTo());
		response.setStatus(msg.getStatus());
		response.setErrorMessage(msg.getErrorMessage());
		try{
		response.setErrorCode(msg.getErrorCode());
		}
		catch(IllegalArgumentException ie){
			response.setErrorCode(null);
		}
		if(msg.getDateCreated()!=null)
			response.setDateCreated(msg.getDateCreated().toString());
		if(msg.getDateSent()!=null)
			response.setDateSent(msg.getDateSent().toString());
		if(msg.getDateUpdated()!=null)
			response.setDateUpdated(msg.getDateUpdated().toString());
		response.setMessage(msg.getBody());
		response.setNumberOfmedia(msg.getNumMedia());
		return response;
	}

	public SMSListResponse formGETResponse(Map<String, Message> tpaasMsgIdMapping, Integer maxNumberSMS) {
		SMSListResponse listResponse = new SMSListResponse();
		for(String id:tpaasMsgIdMapping.keySet()){
			Message msg = tpaasMsgIdMapping.get(id);			
			listResponse.getSmsList().add(formGetByIdResponse(id,id,msg));
			if(maxNumberSMS!=null && listResponse.getSmsList().size() >= maxNumberSMS)
				return listResponse;
		}
		return listResponse;
	}

	public SMSMediaListResponse formMediaListResponse(String smsId, MediaList readMedia) {
		SMSMediaListResponse listResponse = new SMSMediaListResponse();
		if(readMedia == null)
			return listResponse;
		for(Media media:readMedia.getPageData()){
			listResponse.getMediaList().add(formMediaResponse(smsId,media));
			/*if(listResponse.getMediaList().size() >= maxNumberSMS)
				return listResponse;*/
		}
		return listResponse;
	}

	public SMSMediaResponse formMediaResponse(String smsId, Media media) {
		SMSMediaResponse response = new SMSMediaResponse();
		if(media==null)
			return response;
		response.setId(media.getSid());
		response.setContentType(media.getContentType());
		response.setSmsId(smsId);
		if(media.getDateCreated()!=null)
			response.setDateCreated(media.getDateCreated().toString());
		if(media.getDateUpdated()!=null)
			response.setDateUpdated(media.getDateUpdated().toString());
		return response;
	}

	public SMSNumberListResponse formNumbersGETResponse(List<Number> codes) {
		SMSNumberListResponse response = new SMSNumberListResponse();
		Map<String,SMSNumberResponse> map = new HashMap<String,SMSNumberResponse>();
		SMSNumberResponse res=null;
		for (Number number:codes){
			if(map.containsKey(number.getId().getApplicationId()))
				res=map.get(number.getId().getApplicationId());
			else
			    res=new SMSNumberResponse();
			res.setAppId(number.getId().getApplicationId());
			if(number.getId().getNumberType().equalsIgnoreCase("SH"))
				res.setShortcode(number.getNumber());
			else if(number.getId().getNumberType().equalsIgnoreCase("SM"))
				res.setNumber(number.getNumber());
			map.put(number.getId().getApplicationId(), res);
		}
		for(SMSNumberResponse resp: map.values()){
			response.getShortcodes().add(resp);
		}
		return response;
	}
	
	private String generateId(String sid, boolean msgType) throws APIException {
		String id= null;
		if(msgType)
			id ="TWI-OUT-SH-"+sid+"-"+JWTContext.get().getApplicationId();		
		else
			id = "TWI-OUT-SM-"+sid+"-"+JWTContext.get().getApplicationId();
		//byte[]   bytesEncoded = Base64.encodeBase64(id.getBytes());
		//String encodedId = new String(bytesEncoded );
		//System.out.println("ecncoded value is " + new String(bytesEncoded ));
		return id;
	}

	private String encodeId(String sid) {
		byte[]   bytesEncoded = Base64.encodeBase64(sid.getBytes());
		String encodedId = new String(bytesEncoded );
		System.out.println("ecncoded value is " + new String(bytesEncoded ));
		return encodedId;
	}
	private String generateErrorId(boolean msgType) throws APIException {
		String id= null;
		Random rnd = new Random();
		int errId = 100000 + rnd.nextInt(900000);
		if(msgType)
			id ="TWI-OUT-SH-TPAASERR"+errId+"-"+JWTContext.get().getApplicationId();		
		else
			id = "TWI-OUT-SM-TPAASERR"+errId+"-"+JWTContext.get().getApplicationId();
		//byte[]   bytesEncoded = Base64.encodeBase64(id.getBytes());
		//String encodedId = new String(bytesEncoded );
		//System.out.println("ecncoded value is " + new String(bytesEncoded ));
		return id;
	}
	public SendSMSListResponse formPOSTResponse(List<Message> messages, Map<String, String> errorObjects, boolean msgType) throws HibernateException, APIException {
		SendSMSListResponse listResponse = new SendSMSListResponse();
		for (Message msg:messages){
			SendSMSResponse response = new SendSMSResponse();
			String tpaasId = generateId(msg.getSid(),msgType);
			String msgId = generateRandomId();
			response.setId(msgId);
			response.setTo(msg.getTo());
			response.setStatus(msg.getStatus());
			response.setErrorMessage(msg.getErrorMessage());
			try{
			if(msg.getErrorCode()!=null)
				response.setErrorCode(msg.getErrorCode());
			}
			catch(IllegalArgumentException ie){
				response.setErrorCode(null);
			}
			listResponse.getSentSMSList().add(response);
			new DatabaseUtil().writeSentMessageDetails(tpaasId,response);
		}
		for(String to: errorObjects.keySet()){
			SendSMSResponse response = new SendSMSResponse();
			String tpaasId = generateErrorId(msgType);
			String msgId= generateRandomId();
			response.setId(msgId);
			response.setTo(to);
			response.setErrorMessage(errorObjects.get(to));
			response.setStatus("FAILED");	
			listResponse.getSentSMSList().add(response);
			new DatabaseUtil().writeSentMessageDetails(tpaasId,response);
		}
		return listResponse;
	}

	private String generateRandomId() {
		return java.util.UUID.randomUUID().toString();
	}

	public SMSDetailResponse formGetByIdErrResponse(String tpaasId , String smsId, Error err) {
		SMSDetailResponse response = new SMSDetailResponse();
		response.setId(smsId);
		if(tpaasId.contains("-IN-"))
			response.setType(TypeEnum.Incoming);
		else
			response.setType(TypeEnum.Outgoing);
		if(tpaasId.contains("-SH-"))
			response.setSentUsing("Shortcode");
		response.setTo(err.getTo());
		response.setDateSent(err.getDateSent().toString());
		response.setDateCreated(err.getDateCreated().toString());
		response.setDateUpdated(err.getDateUpdated().toString());
		response.setErrorMessage(err.getErrorMessage());
		response.setStatus("FAILED");
		return response;
	}

	public SendNumberResponse formNumberPOSTResponse(SendNumberRequest sendshortcodeRequest) {
		SendNumberResponse response = new SendNumberResponse();
		response.setId(sendshortcodeRequest.getAppId());
		return response;
	}

	public SMSNumberResponse formNumberGETByIdResponse(List<Number> code) throws APIException {
		SMSNumberResponse res = new SMSNumberResponse();
		if(code==null || code.isEmpty()) {
			logger.error("No numbers registered for the application");
			throw new APIException("No numbers registered for the application", Constants.HTTP_STATUS_NOT_FOUND);
		}
		for(Number number:code){
		res.setAppId(number.getId().getApplicationId());
		if(number.getId().getNumberType().equalsIgnoreCase("SH"))
			res.setShortcode(number.getNumber());
		else if(number.getId().getNumberType().equalsIgnoreCase("SM"))
			res.setNumber(number.getNumber());
		}
		return res;
	}
}

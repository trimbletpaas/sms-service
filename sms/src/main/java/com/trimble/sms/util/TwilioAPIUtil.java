package com.trimble.sms.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.hibernate.HibernateException;

import com.trimble.apiutil.context.JWTContext;
import com.trimble.apiutil.exception.APIException;
import com.trimble.apiutil.model.JSONProperty;
import com.trimble.sms.dao.representations.UsageArchive;
import com.trimble.sms.representations.ListSMSUsageDetailResponse;
import com.trimble.sms.representations.SMSUsageDetailResponse;
import com.trimble.sms.representations.SMSUsageResponse;
import com.trimble.sms.representations.SendSMSListResponse;
import com.trimble.sms.representations.SendSMSRequest;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Media;
import com.twilio.sdk.resource.instance.Message;
import com.twilio.sdk.resource.list.MediaList;
import com.twilio.sdk.resource.list.MessageList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TwilioAPIUtil {
	private static Properties properties = JSONProperty.commonProperties;
	private static final Logger logger = LoggerFactory.getLogger(TwilioAPIUtil.class);
	public SendSMSListResponse sendMessage(SendSMSRequest sendSMSRequest) throws HibernateException, APIException{
		SendSMSListResponse response =null;
		Map<String,String> errorObjects = new HashMap<String,String>();
		TwilioRestClient client = new TwilioRestClient(properties.getProperty("twilio.account.id"), properties.getProperty("twilio.account.authtoken")); 
		List<Message> messages = new ArrayList<Message>();
		 // Build the parameters 
		for (String to:sendSMSRequest.getTo()) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			if (to == null)
				continue;
			if (!new ValidationUtil().validatePhone(to)) {
				errorObjects.put(to, "Invalid Phone Number");
			} else{
				params.add(new BasicNameValuePair("To", to.trim()));
			if (sendSMSRequest.getUseShortCode()) {
				String code = new DatabaseUtil().checkIfNumberExists(JWTContext.get().getApplicationId(), "SH");
				code = (code == null) ? properties.getProperty("twilio.common.shortcode") : code;
				params.add(new BasicNameValuePair("From", code));
			} else {
				String number = new DatabaseUtil().checkIfNumberExists(JWTContext.get().getApplicationId(), "SM");
				number = (number == null) ? properties.getProperty("twilio.common.number") : number;
				params.add(new BasicNameValuePair("From", number));
			}

			params.add(new BasicNameValuePair("Body", sendSMSRequest.getMessage()));
			if (sendSMSRequest.getMediaURL() != null && !sendSMSRequest.getMediaURL().isEmpty())
				params.add(new BasicNameValuePair("MediaUrl", sendSMSRequest.getMediaURL()));

			MessageFactory messageFactory = client.getAccount().getMessageFactory();
			Message message = null;
			try {
				message = messageFactory.create(params);
				logger.info("SMS sent from application {} to {} through provider {} ",JWTContext.get().getApplicationId(),to, Constants.TWILIO_PROVIDER);
				System.out.println(message.getPrice() + "-" + message.getPriceUnit());
			} catch (TwilioRestException e) {
				logger.error("Exception occurred while calling Twilio SDK: {}", e.getMessage());
				e.printStackTrace();
				errorObjects.put(to, e.getMessage());
			}
			if (message != null)
				messages.add(message);
		}
		}
		 response = new RequestResponseUtil().formPOSTResponse(messages, errorObjects,sendSMSRequest.getUseShortCode());
		 updateUsage();
		 return response;
	}
	private void updateUsage() throws HibernateException, APIException {
		
		new DatabaseUtil().updateUsage(0, 0);
		
	}
	public Message getMessage(String smsId) throws APIException {
		if(extractTwilioId(smsId).contains("TPAASERR"))
			return null;
		TwilioRestClient client = new TwilioRestClient(properties.getProperty("twilio.account.id"), properties.getProperty("twilio.account.authtoken"));
		String twilioMsgId = extractTwilioId(smsId);
		Message message = client.getAccount().getMessage(twilioMsgId); 
		if(message==null) {
			logger.error("No such message exists with the message ID inputted.");
			throw new APIException("No such message exists with the message ID inputted.", Constants.HTTP_STATUS_NOT_FOUND);
		}
		System.out.println(message.getPrice()+"-"+message.getPriceUnit()); 
		return message;
	}
	private String extractTwilioId(String smsId) {
		//byte[]   bytesDecoded = Base64.decodeBase64(smsId.getBytes());
		//String decodedId = new String(bytesDecoded );
		//System.out.println("ecncoded value is " + decodedId);
		String [] idSegs = smsId.split("-");
		if(idSegs.length>=4)
			return idSegs[3];
		else
			return smsId;
	}
	public MessageList readMessageList(String from, String to, String dateSent) {
		TwilioRestClient client = new TwilioRestClient(properties.getProperty("twilio.account.id"), properties.getProperty("twilio.account.authtoken"));
		Map<String,String> params = new HashMap<String,String>();    
		if(from!=null && !from.isEmpty()) 
			params.put("From", from);
		if(to!=null && !to.isEmpty()) 
			params.put("To", to);
		if(dateSent!=null && !dateSent.isEmpty()) 
			params.put("DateSent", dateSent);
		MessageList messages =null;
		if(params.keySet().size()==0)
			 messages = client.getAccount().getMessages();
		else
			messages = client.getAccount().getMessages(params);
		return messages;
	}
	public MediaList getMediasOfAMessage(String smsId) throws APIException {
		TwilioRestClient client = new TwilioRestClient(properties.getProperty("twilio.account.id"), properties.getProperty("twilio.account.authtoken"));
		String twilioMsgId = extractTwilioId(smsId);
		Message msg = client.getAccount().getMessage(twilioMsgId);
		if(msg==null) {
			logger.error("No such message exists with the message ID inputted.");
			throw new APIException("No such message exists with the message ID inputted.", Constants.HTTP_STATUS_NOT_FOUND);
		}
		MediaList mediaList = null;
		if(msg.getNumMedia()>0)
			mediaList = msg.getMedia(); 
		return mediaList;
	}
	public Media getAMediaMessage(String smsId, String mediaId) throws APIException {
		TwilioRestClient client = new TwilioRestClient(properties.getProperty("twilio.account.id"), properties.getProperty("twilio.account.authtoken"));
		String twilioMsgId = extractTwilioId(smsId);
		Message msg = client.getAccount().getMessage(twilioMsgId);
		if(msg==null) {
			logger.error("No such message exists with the message ID inputted.");
			throw new APIException("No such message exists with the message ID inputted.", Constants.HTTP_STATUS_NOT_FOUND);
		}
		Media media = null;
		if(msg.getNumMedia()>0)
			media = msg.getMedia(mediaId); 
		else {
			logger.error("No such media exists with the media ID inputted.");
			throw new APIException("No such media exists by media ID inputted", Constants.HTTP_STATUS_NOT_FOUND);
		}
		return media;
	}
	public  SMSUsageResponse getUsageToday() throws HibernateException, APIException {
		List<String> twilioIds = new DatabaseUtil().getUsageToday("TWI");
		return formUsageResponse(twilioIds);
	}
	
	private SMSUsageResponse formUsageResponse(List<String> twilioIds) {
		TwilioRestClient client = new TwilioRestClient(properties.getProperty("twilio.account.id"), properties.getProperty("twilio.account.authtoken"));
		int count = 0;
		double totalAmount = 0;
		String priceUnit=null;
		for(String id: twilioIds){
			String providerId=extractTwilioId(id);
			if(providerId.startsWith("TPAASERR"))
				continue;
			Message msg = client.getAccount().getMessage(providerId); 
			if(msg==null)
				continue;
			if(msg.getPrice()!=null && !msg.getPrice().isEmpty()){
				String price = null;
				if(msg.getPrice().startsWith("-"))
					 price = msg.getPrice().substring(1);
			    double amount = (price==null)?0:Double.valueOf(price);
			    totalAmount+=amount;
			    count++;
			    priceUnit=msg.getPriceUnit();
			}
		}
		SMSUsageResponse response= new SMSUsageResponse();
		response.setPrice(totalAmount);
		response.setMessageCount(count);
		response.setPriceUnit(priceUnit);
		response.setStartDate(new Date().toString());
		response.setEndDate(new Date().toString());
		return response;
	}
	public SMSUsageResponse getUsageYesterday() throws HibernateException, APIException {
		List<String> twilioIds = new DatabaseUtil().getUsageYesterday("TWI");
		return formUsageResponse(twilioIds);
	}

	public void getUsageToday(String appId) throws HibernateException, APIException {
		List<String> twilioIds = new DatabaseUtil().getUsageToday(appId, "TWI");
		SMSUsageResponse response = formUsageResponse(twilioIds);
		response.setAppId(appId);
		new DatabaseUtil().updateUsage(response);
	}
	public SMSUsageResponse getUsageThisMonth() throws HibernateException, APIException {
		UsageArchive usage = new DatabaseUtil().getUsageThisMonth();
		SMSUsageResponse response = formUsageResponse(usage);
		Calendar date = Calendar.getInstance();
		date.set(Calendar.DAY_OF_MONTH, 1);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		response.setStartDate(formatter.format(date.getTime()).toString());
		date = Calendar.getInstance();	
		if( date.get(Calendar.DATE) != date.getActualMinimum(Calendar.DATE) )
			date.add(Calendar.DATE,-1);
		response.setEndDate(formatter.format(date.getTime()).toString());
		return response;
	}
	private SMSUsageResponse formUsageResponse(UsageArchive usage) throws APIException {
		SMSUsageResponse response= new SMSUsageResponse();
		if(usage==null) {
			logger.error("No Usage found for the month requested.");
			throw new APIException("No Usage found for the month requested.", Constants.HTTP_STATUS_NOT_FOUND);
		}
		response.setPrice(usage.getTotalAmount());
		response.setMessageCount(usage.getNumbOfMessagesSent());
		response.setPriceUnit("USD");
		response.setAppId(usage.getId().getApplicationId());
		response.setAppName(usage.getApplicationName());
		response.setOwner(usage.getApplicationOwner());
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		response.setStartDate(formatter.format(new Date()));
		response.setEndDate(formatter.format(new Date()));
		return response;
	}
	public SMSUsageResponse getUsageLastMonth() throws HibernateException, APIException {
		UsageArchive usage = new DatabaseUtil().getUsageLastMonth();
		SMSUsageResponse response = formUsageResponse(usage);
		Calendar date = Calendar.getInstance();
		date.set(Calendar.DAY_OF_MONTH, 1);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		response.setStartDate(formatter.format(date.getTime()).toString());
		return response;
	}
	public SMSUsageDetailResponse getUsage() throws APIException {
		List<UsageArchive> usages = new DatabaseUtil().getUsage();
		SMSUsageDetailResponse response = formUsageDetailResponse(JWTContext.get().getApplicationId(),JWTContext.get().getApplicationName(),JWTContext.get().getSubscriber(),usages);
		/*Calendar date = Calendar.getInstance();
		date.set(Calendar.DAY_OF_MONTH, 1);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		response.setStartDate(formatter.format(date.getTime()).toString());*/
		return response;
	}
	private SMSUsageDetailResponse formUsageDetailResponse(String appId, String appName, String owner, List<UsageArchive> usages) {
		SMSUsageDetailResponse response = new SMSUsageDetailResponse();
		response.setAppId(appId);
		response.setAppName(appName);
		response.setOwner(owner);
		response.setPriceUnit("USD");
		int totalMessageCount = 0;
		double totalAmount = 0;
		for(UsageArchive usage:usages){
			response.setUsage(usage.getNumbOfMessagesSent(), usage.getTotalAmount(), usage.getId().getUsageForMonth());
			totalMessageCount+=usage.getNumbOfMessagesSent();
			totalAmount+=usage.getTotalAmount();
		}
		response.setTotalAmount(totalAmount);
		response.setTotalMessageCount(totalMessageCount);
		return response;
	}
	public ListSMSUsageDetailResponse getAllUsage() throws HibernateException, APIException {
		Map<String,List<UsageArchive>> usages = new DatabaseUtil().getAllUsage();
		ListSMSUsageDetailResponse response = formAllUsageDetailResponse(usages);
		/*Calendar date = Calendar.getInstance();
		date.set(Calendar.DAY_OF_MONTH, 1);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		response.setStartDate(formatter.format(date.getTime()).toString());*/
		return response;
	}
	private ListSMSUsageDetailResponse formAllUsageDetailResponse(Map<String, List<UsageArchive>> usages) {
		ListSMSUsageDetailResponse responses=new ListSMSUsageDetailResponse();
		List<SMSUsageDetailResponse> detailResponse = new ArrayList<SMSUsageDetailResponse>();
		for(String id:usages.keySet()){
			List<UsageArchive> appUsages = usages.get(id);
		    if(appUsages!=null&appUsages.get(0)!=null){
		    	UsageArchive usage = appUsages.get(0);
		    	SMSUsageDetailResponse response=formUsageDetailResponse(usage.getId().getApplicationId(),usage.getApplicationName(),usage.getApplicationOwner(),appUsages);
		    	detailResponse.add(response);
		    }
		    	
		}
		responses.setUsageDetailsByApplication(detailResponse);
		return responses;
	}
}

package com.trimble.sms.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.hibernate.HibernateException;

import com.trimble.apiutil.exception.APIException;
import com.trimble.sms.dao.impl.SMSDAOImpl;
import com.trimble.sms.dao.representations.Message;
import com.trimble.sms.dao.representations.Number;
import com.trimble.sms.dao.representations.UsageArchive;
import com.trimble.sms.dao.representations.Error;
import com.trimble.sms.representations.SMSUsageResponse;
import com.trimble.sms.representations.SendSMSListResponse;
import com.trimble.sms.representations.SendSMSResponse;
import com.trimble.sms.representations.SendNumberRequest;
import com.twilio.sdk.resource.list.MessageList;

public class DatabaseUtil {
	public void writeSentMessageDetails(SendSMSListResponse sentMessages) throws HibernateException, APIException{
		for(SendSMSResponse msg: sentMessages.getSentSMSList()){
		if(msg==null)
			continue;
		Message dbObject = new Message();
		dbObject.setMessageId(decodeId(msg.getId()));
		dbObject.setProviderReferenceId(extractId(msg.getId()));
		SMSDAOImpl dbInstance = openDBSession();
		if(dbObject.getProviderReferenceId().startsWith("TPAASERR")){
			Error err = new Error();
			err.setTo(msg.getTo());
			err.setErrId(dbObject.getProviderReferenceId());
			err.setErrorMessage(msg.getErrorMessage());
			dbInstance.persistError(err);
		}
		dbInstance.persist(dbObject);
		closeDBSession(dbInstance);
		}	
	}
	
	private String extractId(String smsId) {
		//String decodedId =  decodeId(smsId);
		//System.out.println("ecncoded value is " + decodedId);
		String [] idSegs = smsId.split("-");
		if(idSegs.length>=4)
			return idSegs[3];
		return null;
	}
	
	private String decodeId(String encodedId){
		byte[]   bytesDecoded = Base64.decodeBase64(encodedId.getBytes());
		String decodedId = new String(bytesDecoded );
		return decodedId;
	}
	
	private void closeDBSession(SMSDAOImpl dbInstance) throws APIException {
		dbInstance.closeCurrentSessionwithTransaction();
	}
	private SMSDAOImpl openDBSession() throws HibernateException, APIException {
		SMSDAOImpl dbInstance = new SMSDAOImpl();
		dbInstance.openCurrentSessionwithTransaction();
		return dbInstance;
	}
	
	public Map<String, com.twilio.sdk.resource.instance.Message> readMessageDetails(MessageList readMessages, boolean allRecords, String date) throws HibernateException, APIException {
		List<com.twilio.sdk.resource.instance.Message> msgs = readMessages.getPageData();
		Map<String, com.twilio.sdk.resource.instance.Message> tpaasMsgIdMapping = new HashMap<String, com.twilio.sdk.resource.instance.Message>();
		for (com.twilio.sdk.resource.instance.Message msg:msgs){
			SMSDAOImpl dbInstance = openDBSession();
			String tpaasMsgId =null;
			if(allRecords)
				tpaasMsgId = dbInstance.findAllByReferenceIdNDate(msg.getSid(), date);
			else
				tpaasMsgId = dbInstance.findByReferenceIdNDate(msg.getSid(), date);
			closeDBSession(dbInstance);
			if(tpaasMsgId!=null)
				tpaasMsgIdMapping.put(tpaasMsgId, msg);
		}
		return tpaasMsgIdMapping;
	}
	public String checkIfNumberExists(String appId,String numberType) throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		List<Number> number = dbInstance.findNumberById(appId);
		closeDBSession(dbInstance);
		if (number==null)
			return null;
		for (Number num:number){
			if(num.getId().getNumberType().equalsIgnoreCase(numberType))
				return num.getNumber();
		}
		return null;
	}
	public List<Number> readNumbers() throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		List<Number> shortcode = dbInstance.findAllNumbers();
		closeDBSession(dbInstance);
		return shortcode;		
	}

	public Error readErrorDetails(String smsId) throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		Error err = dbInstance.findErrorById(extractId(smsId));
		closeDBSession(dbInstance);
		return err;	
		
	}

	public void writeNumberDetails(SendNumberRequest sendshortcodeRequest) throws HibernateException, APIException {
		Number code = new Number();
		boolean created=false;
		if(sendshortcodeRequest.getShortcode()!=null && !sendshortcodeRequest.getShortcode().isEmpty()){
			code.setId(sendshortcodeRequest.getAppId(),"SH","TWI");
			code.setNumber(sendshortcodeRequest.getShortcode());
			persistNumber(code);
			created=true;
		}
		if(sendshortcodeRequest.getNumber()!=null && !sendshortcodeRequest.getNumber().isEmpty()){
			code.setId(sendshortcodeRequest.getAppId(),"SM","TWI");
			code.setNumber(sendshortcodeRequest.getNumber());
			if(created)
				updateNumber(code);
			else
				persistNumber(code);
			
		}		
	}

	private void persistNumber(Number code) throws APIException {
		SMSDAOImpl dbInstance = openDBSession();
		if(checkIfAppExists(code.getId().getApplicationId())){
			closeDBSession(dbInstance);
			throw new APIException("Record already exists.", Constants.HTTP_STATUS_CONFLICT);
		}
			
		dbInstance.persistNumber(code);
		closeDBSession(dbInstance);	
		
	}

	private boolean checkIfAppExists(String appId) throws APIException {
		SMSDAOImpl dbInstance = openDBSession();
		List<Number> number = dbInstance.findNumberById(appId);
		closeDBSession(dbInstance);
		return (number!=null&&number.size()!=0)?true:false;
	}

	public void updateNumberDetails(SendNumberRequest sendshortcodeRequest) throws HibernateException, APIException {
		Number code = new Number();
		if(sendshortcodeRequest.getShortcode()!=null && !sendshortcodeRequest.getShortcode().isEmpty()){
			code.setId(sendshortcodeRequest.getAppId(),"SH","TWI");
			code.setNumber(sendshortcodeRequest.getShortcode());
			updateNumber(code);
		}
		if(sendshortcodeRequest.getNumber()!=null && !sendshortcodeRequest.getNumber().isEmpty()){
			code.setId(sendshortcodeRequest.getAppId(),"SM","TWI");
			code.setNumber(sendshortcodeRequest.getNumber());
			updateNumber(code);
		}
		
	}

	private void updateNumber(Number code) throws APIException {
		SMSDAOImpl dbInstance = openDBSession();
		if(!checkIfAppExists(code.getId().getApplicationId())){
			closeDBSession(dbInstance);
			throw new APIException("No such Record Exists", Constants.HTTP_STATUS_NOT_FOUND);
		}
		dbInstance.updateNumber(code);
		closeDBSession(dbInstance);	
	}

	public List<Number> readNumberById(String appId) throws APIException {
		SMSDAOImpl dbInstance = openDBSession();
		List<Number> code = dbInstance.findNumberById(appId);
		closeDBSession(dbInstance);
		return code;	
	}

	public void updateUsage(int count, double totalAmount) throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		dbInstance.updateUsageByApplication(count,totalAmount);
		closeDBSession(dbInstance);
	}

	public List<String> getUsageToday(String provider) throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		List<String> ids = dbInstance.fetchApplicationUsageToday(provider);
		closeDBSession(dbInstance);
		return ids;
	}

	public List<String> getUsageYesterday(String provider) throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		List<String> ids = dbInstance.fetchApplicationUsageYesterday(provider);
		closeDBSession(dbInstance);
		return ids;
	}

	public void updateUsage(SMSUsageResponse response) throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		dbInstance.updateUsageByApplication(response.getAppId(),response.getMessageCount(),response.getPrice());
		closeDBSession(dbInstance);
		
	}

	public List<String> getUsageToday(String appId, String provider) throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		List<String> ids = dbInstance.fetchApplicationUsageToday(appId, provider);
		closeDBSession(dbInstance);
		return ids;
	}

	public UsageArchive getUsageThisMonth() throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		UsageArchive usage = dbInstance.fetchApplicationUsageThisMonth ();
		closeDBSession(dbInstance);
		return usage;
	}

	public List<String> getApplicationIds() throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		List<String> ids = dbInstance.fetchApplicationIds();
		closeDBSession(dbInstance);
		return ids;
	}

	public UsageArchive getUsageLastMonth() throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		UsageArchive usage = dbInstance.fetchApplicationUsageLastMonth ();
		closeDBSession(dbInstance);
		return usage;
	}

	public List<UsageArchive> getUsage() throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		List<UsageArchive> usages = dbInstance.findUsage ();
		closeDBSession(dbInstance);
		return usages;
	}

	public Map<String, List<UsageArchive>> getAllUsage() throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		Map<String, List<UsageArchive>> usages = dbInstance.findAllUsage ();
		closeDBSession(dbInstance);
		return usages;
	}

	public void writeSentMessageDetails(String tpaasId, SendSMSResponse response) throws HibernateException, APIException {
		if(response==null)
			return;
		Message dbObject = new Message();
		dbObject.setMessageId(response.getId());
		dbObject.setProviderReferenceId(tpaasId);
		SMSDAOImpl dbInstance = openDBSession();
		if(extractId(dbObject.getProviderReferenceId()).startsWith("TPAASERR")){
			Error err = new Error();
			err.setTo(response.getTo().replace("-","").replace(" ","").replace("(","").replace(")",""));
			err.setErrId(extractId(dbObject.getProviderReferenceId()));
			err.setErrorMessage(response.getErrorMessage());
			dbInstance.persistError(err);
		}
		dbInstance.persist(dbObject);
		closeDBSession(dbInstance);
		
	}

	public String fetchReferenceId(String smsId) throws HibernateException, APIException {
		SMSDAOImpl dbInstance = openDBSession();
		String reference = dbInstance.fetchReferenceId(smsId);
		closeDBSession(dbInstance);
		return reference;
	}

	public void DeleteNumberDetails(String appId) throws APIException {
		SMSDAOImpl dbInstance = openDBSession();
		if(!checkIfAppExists(appId)){
			closeDBSession(dbInstance);
			throw new APIException("No such Record Exists", Constants.HTTP_STATUS_NOT_FOUND);
		}
		dbInstance.deleteNumber(appId);
		closeDBSession(dbInstance);
	}
}

package com.trimble.sms.dao;

import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.trimble.apiutil.commons.Messages;
import com.trimble.apiutil.exception.APIException;
import com.trimble.apiutil.model.JSONProperty;

public class DAOImpl {
	private Session currentSession;
	private Transaction currentTransaction;
	private static SessionFactory sessionFactory = null;
	private static Properties connectionProperties = null;

	public Session openCurrentSession() throws HibernateException, APIException {
		if(getSessionFactory()!=null){
			currentSession = getSessionFactory().openSession();
		}
		return currentSession;

	}

	public Session openCurrentSessionwithTransaction() throws HibernateException, APIException {
			currentSession = getSessionFactory().openSession();
			currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() throws APIException {
		if(currentSession!=null && currentSession.isOpen())
			currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() throws APIException {
		if(currentTransaction!=null ) 
			currentTransaction.commit();
		if(currentSession!=null && currentSession.isOpen()) {
			currentSession.close();
		}
	}

	@SuppressWarnings("deprecation")
	private static SessionFactory getSessionFactory() throws APIException {
		if (sessionFactory == null){
			Configuration configuration = new Configuration().configure();
			if(connectionProperties == null)
				connectionProperties = JSONProperty.environmentProperties;
			Properties p = new Properties();
			
			p.setProperty(Messages.getString("Message.HIBERNATE_DIALECT"), connectionProperties.getProperty(Messages.getString("Message.JSON_PROP_HIBERNATE_DIALECT"))); //$NON-NLS-1$ //$NON-NLS-2$
			p.setProperty(Messages.getString("Message.HIBERNATE_DRIVER_CLASS"), connectionProperties.getProperty(Messages.getString("Message.JSON_PROP_HIBERNATE_DRIVER"))); //$NON-NLS-1$ //$NON-NLS-2$
			p.setProperty(Messages.getString("Message.HIBERNATE_CONN_URL"), connectionProperties.getProperty(Messages.getString("Message.JSON_PROP_HIBERNATE_URL"))); //$NON-NLS-1$ //$NON-NLS-2$
			p.setProperty(Messages.getString("Message.HIBERNATE_CONN_USER"), connectionProperties.getProperty(Messages.getString("Message.JSON_PROP_HIBERNATE_USER"))); //$NON-NLS-1$ //$NON-NLS-2$
			p.setProperty(Messages.getString("Message.HIBERNATE_CONN_PWD"), connectionProperties.getProperty(Messages.getString("Message.JSON_PROP_HIBERNATE_PWD"))); //$NON-NLS-1$ //$NON-NLS-2$
			
			if(configuration!=null){
				configuration.addProperties(p);
				sessionFactory = configuration.buildSessionFactory();// builder.build());
			}
			
		}
		return sessionFactory;
	}


	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}
}

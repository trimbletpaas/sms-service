package com.trimble.sms.dao.representations;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "SMS_APPLICATION")
public class Application {

	@Id
	@Column(name = "ID")
	private String applicationId;
	@Column(name = "NAME")
	String applicationName;
	@Column(name = "NUM_SMS_SENT")
	int numbOfMessagesSent;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "DATE_CREATED")
	Date dateCreated=new Date();
	@Column(name = "USAGE_AMOUNT")
	double totalAmount;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "DATE_UPDATED")
	Date dateUpdated=new Date();
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public int getNumbOfMessagesSent() {
		return numbOfMessagesSent;
	}
	public void setNumbOfMessagesSent(int numbOfMessagesSent) {
		this.numbOfMessagesSent = numbOfMessagesSent;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	@Override
	public String toString() {
		return "Application [applicationId=" + applicationId + ", applicationName=" + applicationName
				+ ", numbOfMessagesSent=" + numbOfMessagesSent + ", dateCreated=" + dateCreated + ", totalAmount="
				+ totalAmount + ", dateUpdated=" + dateUpdated + "]";
	}
	
	
}

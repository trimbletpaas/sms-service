package com.trimble.sms.dao.representations;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "SMS_MESSAGE_ERROR")
public class Error {
	@Id
	@Column(name = "ID")
	private String errId;
	@Column(name = "ERROR_MESSAGE")
	String errorMessage;
	@Column(name = "RECIPIENT")
	String recipient;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "DATE_CREATED")
	Date dateCreated=new Date();
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "DATE_SENT")
	Date dateSent=new Date();
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "DATE_UPDATED")
	Date dateUpdated=new Date();
	
	
	public String getTo() {
		return recipient;
	}
	public void setTo(String recipient) {
		this.recipient = recipient;
	}
	public Date getDateSent() {
		return dateSent;
	}
	public void setDateSent(Date dateSent) {
		this.dateSent = dateSent;
	}
	public String getErrId() {
		return errId;
	}
	public void setErrId(String errId) {
		this.errId = errId;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	@Override
	public String toString() {
		return "Error [errId=" + errId + ", errorMessage=" + errorMessage + ", to=" + recipient + ", dateCreated="
				+ dateCreated + ", dateSent=" + dateSent + ", dateUpdated=" + dateUpdated + "]";
	}

		
}

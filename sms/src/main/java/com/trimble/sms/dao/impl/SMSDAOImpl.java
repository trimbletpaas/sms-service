package com.trimble.sms.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;

import com.trimble.apiutil.context.JWTContext;
import com.trimble.apiutil.exception.APIException;
import com.trimble.sms.dao.DAOImpl;
import com.trimble.sms.dao.DAOInterface;
import com.trimble.sms.dao.representations.Error;
import com.trimble.sms.dao.representations.Message;
import com.trimble.sms.dao.representations.Number;
import com.trimble.sms.dao.representations.UsageArchive;

public class SMSDAOImpl extends DAOImpl implements DAOInterface<Message, String> {

	private static final String FIND_ALL_MESSAGES = "from Message";
	private static final String FIND_ALL_SHORTCODES = "from Number";
	private static final String FIND_SHORTCODE_BY_ID = "from Number num where num.id.applicationId=:appId";
	private static final String FIND_ALL_APPLICATION_IDS = "from UsageArchive usage where usage.id.usageForMonth=:usageMonth";
	private static final String FIND_ALL_APPLICATION_IDS_ALL_USAGE = "from UsageArchive";
	private static final String FIND_MESSAGE_ALL_BY_REF_ID = "from Message where providerReferenceId like :refId";
	private static final String FIND_MESSAGE_ALL_BY_REF_ID_N_DATE = "from Message where providerReferenceId like :refId and DATE(dateCreated) = DATE(:dateString)";
	private static final String FIND_MESSAGE_BY_REF_ID = "from Message where providerReferenceId like :searchString";
	private static final String FIND_MESSAGE_BY_REF_ID_N_DATE = "from Message where DATE(dateCreated)=DATE(:dateString) and providerReferenceId like :searchString";
	private static final String FIND_MESSAGES_TODAY= "select providerReferenceId from Message where DATE(dateCreated) = CURDATE() and  providerReferenceId like :searchString";
	private static final String FIND_MESSAGES_YESTERDAY= "select providerReferenceId from Message where DATE(dateCreated) = CURDATE()-1 and  providerReferenceId like :searchString";
	private static final String FIND_APP_IN_USAGE = "from UsageArchive usage where usage.id.applicationId=:appId and usage.id.usageForMonth=:usageMonth";
	private static final String FIND_USAGE_BYMONTH= "from UsageArchive usage where usage.id.applicationId=:appId and usage.id.usageForMonth=:usageMonth";
	private static final String FIND_ALLUSAGE_BYAPPLICATION = "from UsageArchive usage where usage.id.applicationId=:appId";
	private static final String FIND_REFERENCE_ID = "select providerReferenceId from Message where messageId=:id";
	@Override
	public void persist(Message entity) {
		getCurrentSession().persist(entity);
	}

	@Override
	public void update(Message entity) {
		getCurrentSession().update(entity);
		
	}

	@Override
	public Message findById(String id) {
		Message entity = (Message) getCurrentSession().get(Message.class, new String(id));
		return entity;
	}

	@Override
	public boolean delete(String id) {
		Message entity = findById(id);
		if (entity != null) {
			getCurrentSession().delete(entity);
		} else {
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> findAll() {
		List<Message> entities = (List<Message>) getCurrentSession().createQuery(FIND_ALL_MESSAGES).list();
		return entities;
	}

	@Override
	public void deleteAll() {
		deleteAll();
		
	}
	
	public String findAllByReferenceId(String refId) {
		if(refId==null)
			return null;
		Query query = getCurrentSession().createQuery(FIND_MESSAGE_ALL_BY_REF_ID);
		query.setString("refId", "%-"+refId+"-%");
		Message msg = ((Message) query.uniqueResult());
		if (msg!=null)
			return msg.getMessageId();
		else
			return null;
	}

	public String findAllByReferenceIdNDate(String refId, String date) {
		if(refId==null)
			return null;
		Query query = getCurrentSession().createQuery(FIND_MESSAGE_ALL_BY_REF_ID_N_DATE);
		query.setString("refId", "%-"+refId+"-%");
		query.setString("dateString", date);
		Message msg = ((Message) query.uniqueResult());
		if (msg!=null)
			return msg.getMessageId();
		else
			return null;
	}


	public String findByReferenceId(String refId) throws APIException {
		if(refId==null)
			return null;
		if(JWTContext.get()==null)
			throw new APIException("Application Context is not available.JWT Header is missing.");
		Query query = getCurrentSession().createQuery(FIND_MESSAGE_BY_REF_ID);
		//query.setString("refId", refId);
		query.setString("searchString", "%-"+refId+"-"+JWTContext.get().getApplicationId());
		Message msg = ((Message) query.uniqueResult());
		if (msg!=null)
			return msg.getMessageId();
		else
			return null;
	}
	public String findByReferenceIdNDate(String refId, String date) throws APIException {
		if(refId==null)
			return null;
		if(JWTContext.get()==null)
			throw new APIException("Application Context is not available.JWT Header is missing.");
		Query query = getCurrentSession().createQuery(FIND_MESSAGE_BY_REF_ID_N_DATE);
		//query.setString("refId", refId);
		query.setString("dateString", date);
		query.setString("searchString", "%-"+refId+"-"+JWTContext.get().getApplicationId());
		Message msg = ((Message) query.uniqueResult());
		if (msg!=null)
			return msg.getMessageId();
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	public List<Number> findNumberById(String applicationId) {
		List<Number> entity = (List<Number>) getCurrentSession().createQuery(FIND_SHORTCODE_BY_ID).setParameter("appId", applicationId).list();
		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<Number> findAllNumbers() {
		List<Number> entities = (List<Number>) getCurrentSession().createQuery(FIND_ALL_SHORTCODES).list();
		return entities;
	}

	public void persistError(Error err) {
		getCurrentSession().persist(err);		
	}

	public Error findErrorById(String smsId) {
		Error entity = (Error) getCurrentSession().get(Error.class, new String(smsId));
		return entity;
	}

	public void persistNumber(Number code) {
		getCurrentSession().persist(code);
		
	}

	public void updateNumber(Number code) {
		getCurrentSession().saveOrUpdate(code);
		
	}

	public void updateUsageByApplication(int count, double totalAmount) throws APIException {
		if(JWTContext.get()==null)
			throw new APIException("Application Context is not available.JWT Header is missing.");
		updateUsageByApplication(JWTContext.get().getApplicationId(),count,totalAmount);
		
	}
	public void updateUsageByApplication(String appId, int count, double totalAmount) throws APIException {
		SimpleDateFormat formatter = new SimpleDateFormat("MMMYYYY");
		System.out.println(new Date().toString());
		String currentMonth = formatter.format(new Date());
		UsageArchive app = findApplicationById(appId,currentMonth);
		if(app!=null){
			if(count==0 && totalAmount==0)
				return;
			app.setNumbOfMessagesSent(count+app.getNumbOfMessagesSent());
			app.setTotalAmount(totalAmount+app.getTotalAmount());	
			app.setDateUpdated(new Date());
			getCurrentSession().update(app);
		}
		else{
			app=new UsageArchive();
			app.setId(appId, currentMonth);
			app.setApplicationName(JWTContext.get().getApplicationName());
			app.setApplicationOwner(JWTContext.get().getEndUser());
			app.setNumbOfMessagesSent(count);
			app.setTotalAmount(totalAmount);
			getCurrentSession().persist(app);
		}
	}

	private UsageArchive findApplicationById(String applicationId, String month) {
		Query query = getCurrentSession().createQuery(FIND_APP_IN_USAGE);
		query.setParameter("appId", applicationId);
		query.setParameter("usageMonth", month);
		UsageArchive entity = (UsageArchive) query.uniqueResult();
		return entity;
	}

	public List<String> fetchApplicationUsageToday(String provider) throws APIException {
		if(JWTContext.get()==null)
			throw new APIException("Application Context is not available.JWT Header is missing.");
		return fetchApplicationUsage(JWTContext.get().getApplicationId(),FIND_MESSAGES_TODAY,provider);
	}

	public List<String> fetchApplicationUsageToday(String appId, String provider) {
		return fetchApplicationUsage(appId,FIND_MESSAGES_TODAY,provider);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> fetchApplicationUsage(String appId,String queryString, String provider){
		Query query = getCurrentSession()
				.createQuery(queryString);
		query.setParameter("searchString", provider+"-%-"+appId);
		List<String> entities = (List<String>) query.list();
		System.out.println(entities.size());
		return entities;
	}
	
	
	public List<String> fetchApplicationUsageYesterday(String provider) throws APIException {
		return fetchApplicationUsage(JWTContext.get().getApplicationId(),FIND_MESSAGES_YESTERDAY,provider);
	}

	public UsageArchive fetchApplicationUsageThisMonth() throws APIException {
		return fetchMonthlyUsage(JWTContext.get().getApplicationId(),FIND_USAGE_BYMONTH,new Date());
	}

	private UsageArchive fetchMonthlyUsage(String applicationId, String findUsageThismonth, Date date) {
		Query query = getCurrentSession()
				.createQuery(findUsageThismonth);
		query.setParameter("appId", applicationId);
		SimpleDateFormat formatter = new SimpleDateFormat("MMMYYYY");		
		query.setParameter("usageMonth", formatter.format(date));
		UsageArchive entity = (UsageArchive) query.uniqueResult();
		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<String> fetchApplicationIds() {
		List<String> ids = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("MMMYYYY");
		System.out.println(new Date().toString());
		List<UsageArchive> usages = (List<UsageArchive>) getCurrentSession().createQuery(FIND_ALL_APPLICATION_IDS).setParameter("usageMonth", formatter.format(new Date())).list();
		for (UsageArchive usage: usages){
			ids.add(usage.getId().getApplicationId());
		}
		return ids;
	}

	public UsageArchive fetchApplicationUsageLastMonth() throws APIException {
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		aCalendar.add(Calendar.MONTH, -1);
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);

		System.out.println(aCalendar.getTime().toString());
		if(JWTContext.get()==null)
			throw new APIException("Application Context is not available.JWT Header is missing.");
		return fetchMonthlyUsage(JWTContext.get().getApplicationId(),FIND_USAGE_BYMONTH,aCalendar.getTime());
	}
	
	@SuppressWarnings("unchecked")
	public List<UsageArchive> findUsage(String appId,String queryString){
		Query query = getCurrentSession()
				.createQuery(queryString);
		query.setParameter("appId", appId);
		List<UsageArchive> entities = (List<UsageArchive>) query.list();
		return entities;
	}

	public List<UsageArchive> findUsage() throws APIException{
		if(JWTContext.get()==null)
			throw new APIException("Application Context is not available.JWT Header is missing.");
		return findUsage(JWTContext.get().getApplicationId(),FIND_ALLUSAGE_BYAPPLICATION);
	}

	public Map<String, List<UsageArchive>> findAllUsage() {
		Map<String,List<UsageArchive>> map = new HashMap<String,List<UsageArchive>>();
		List<String> appIds = fetchAllApplicationIds();
		for(String appId:appIds){
			List<UsageArchive> usages= findUsage(appId,FIND_ALLUSAGE_BYAPPLICATION);
			if(usages==null)
				usages=new ArrayList<UsageArchive>();
			map.put(appId, usages);				
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	private List<String> fetchAllApplicationIds() {
		List<String> ids = new ArrayList<String>();
		Set<String> uniqueIds = new HashSet<String>();
		List<UsageArchive> usages = (List<UsageArchive>) getCurrentSession().createQuery(FIND_ALL_APPLICATION_IDS_ALL_USAGE).list();
		for (UsageArchive usage: usages){
			uniqueIds.add(usage.getId().getApplicationId());
		}
		ids.addAll(uniqueIds);
		return ids;
	}

	public String fetchReferenceId(String smsId) {
		String referenceId=(String) getCurrentSession().createQuery(FIND_REFERENCE_ID).setParameter("id",smsId).uniqueResult();
		return referenceId;
	}

	public void deleteNumber(String appId) {
		List<Number> numbers = findNumberById(appId);
		for(Number number: numbers)
			getCurrentSession().delete(number);
	}
}

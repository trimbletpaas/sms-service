package com.trimble.sms.dao.representations;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "SMS_USAGE_ARCHIVE")
public class UsageArchive {
	@EmbeddedId
	Primary id;
	@Column(name = "NAME")
	String applicationName;
	@Column(name = "owner")
	String applicationOwner;
	@Column(name = "NUM_SMS_SENT")
	int numbOfMessagesSent;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "DATE_CREATED")
	Date dateCreated=new Date();
	@Column(name = "USAGE_AMOUNT")
	double totalAmount;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "DATE_UPDATED")
	Date dateUpdated=new Date();

	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public int getNumbOfMessagesSent() {
		return numbOfMessagesSent;
	}
	public void setNumbOfMessagesSent(int numbOfMessagesSent) {
		this.numbOfMessagesSent = numbOfMessagesSent;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
	public String getApplicationOwner() {
		return applicationOwner;
	}
	public void setApplicationOwner(String applicationOwner) {
		this.applicationOwner = applicationOwner;
	}
	
	
	public Primary getId() {
		return id;
	}
	public void setId(String applicationId, String usageForMonth) {
		this.id = new UsageArchive.Primary();
		this.id.applicationId = applicationId;
		this.id.usageForMonth = usageForMonth;
	}


	@Override
	public String toString() {
		return "UsageArchive [id=" + id + ", applicationName=" + applicationName + ", applicationOwner="
				+ applicationOwner + ", numbOfMessagesSent=" + numbOfMessagesSent + ", dateCreated=" + dateCreated
				+ ", totalAmount=" + totalAmount + ", dateUpdated=" + dateUpdated + "]";
	}


	@Embeddable
	public static class Primary implements java.io.Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		@Column(name = "APPID")
		private String applicationId;
		@Column(name = "USAGE_MONTH")
		String usageForMonth;

		
		
		public String getApplicationId() {
			return applicationId;
		}

		public void setApplicationId(String applicationId) {
			this.applicationId = applicationId;
		}

		public String getUsageForMonth() {
			return usageForMonth;
		}

		public void setUsageForMonth(String usageForMonth) {
			this.usageForMonth = usageForMonth;
		}

		public Primary() {

		}

		public Primary(String applicationId, String usageForMonth) {
			this.applicationId = applicationId;
			this.usageForMonth = usageForMonth;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((applicationId == null) ? 0 : applicationId.hashCode());
			result = prime * result + ((usageForMonth == null) ? 0 : usageForMonth.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Primary other = (Primary) obj;
			if (applicationId == null) {
				if (other.applicationId != null)
					return false;
			} else if (!applicationId.equals(other.applicationId))
				return false;
			if (usageForMonth == null) {
				if (other.usageForMonth != null)
					return false;
			} else if (!usageForMonth.equals(other.usageForMonth))
				return false;
			return true;
		}

	}
}

package com.trimble.sms.dao.representations;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "SMS_MESSAGE")
public class Message {
	@Id
	@Column(name = "ID")
	private String messageId;
	@Column(name = "REFERENCE_ID")
	String providerReferenceId;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "DATE_CREATED")
	Date dateCreated=new Date();
	@Column(name = "DATE_UPDATED")
	Date dateUpdated=new Date();
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getProviderReferenceId() {
		return providerReferenceId;
	}
	public void setProviderReferenceId(String providerReferenceId) {
		this.providerReferenceId = providerReferenceId;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	@Override
	public String toString() {
		return "Message [messageId=" + messageId + ", providerReferenceId=" + providerReferenceId + ", dateCreated="
				+ dateCreated + ", dateUpdated=" + dateUpdated + "]";
	}
	

	
}

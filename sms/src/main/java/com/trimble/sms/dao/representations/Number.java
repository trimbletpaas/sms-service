package com.trimble.sms.dao.representations;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import com.trimble.sms.dao.representations.UsageArchive.Primary;

@Entity
@Table(name = "SMS_SHORTCODE_LIST")
public class Number {
	@EmbeddedId
	Primary id;
	@Column(name = "NUMBER")
	String number;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "DATE_CREATED")
	Date dateCreated=new Date();
	@Column(name = "DATE_UPDATED")
	Date dateUpdated=new Date();
	
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	public Primary getId() {
		return id;
	}
	public void setId(String applicationId, String numberType, String provider) {
		this.id = new Number.Primary();
		this.id.applicationId = applicationId;
		this.id.numberType = numberType;
		this.id.provider= provider;
	}

	
	@Embeddable
	public static class Primary implements java.io.Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		@Column(name = "ID")
		private String applicationId;
		@Column(name = "TYPE")
		private String numberType;
		@Column(name = "PROVIDER")
		private String provider;
		
	
		public String getApplicationId() {
			return applicationId;
		}

		public void setApplicationId(String applicationId) {
			this.applicationId = applicationId;
		}

		public String getNumberType() {
			return numberType;
		}

		public void setNumberType(String numberType) {
			this.numberType = numberType;
		}

		public String getProvider() {
			return provider;
		}

		public void setProvider(String provider) {
			this.provider = provider;
		}

		public Primary() {

		}

		public Primary(String applicationId, String numberType, String provider) {
			this.applicationId = applicationId;
			this.numberType = numberType;
			this.provider = provider;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((applicationId == null) ? 0 : applicationId.hashCode());
			result = prime * result + ((numberType == null) ? 0 : numberType.hashCode());
			result = prime * result + ((provider == null) ? 0 : provider.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Primary other = (Primary) obj;
			if (applicationId == null) {
				if (other.applicationId != null)
					return false;
			} else if (!applicationId.equals(other.applicationId))
				return false;
			if (numberType == null) {
				if (other.numberType != null)
					return false;
			} else if (!numberType.equals(other.numberType))
				return false;
			if (provider == null) {
				if (other.provider != null)
					return false;
			} else if (!provider.equals(other.provider))
				return false;
			return true;
		}

		
	}
}

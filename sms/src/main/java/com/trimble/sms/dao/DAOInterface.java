package com.trimble.sms.dao;

import java.io.Serializable;
import java.util.List;

public interface DAOInterface<T, Id extends Serializable> {
	public void persist(T entity);

	public void update(T entity);

	public T findById(String id);

	public boolean delete(String id);

	public List<T> findAll();

	public void deleteAll();
}

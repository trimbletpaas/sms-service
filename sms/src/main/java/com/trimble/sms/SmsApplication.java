package com.trimble.sms;

import com.trimble.apiutil.config.ApplicationConfig;
import com.trimble.apiutil.exception.APIException;
import com.trimble.apiutil.reader.JSONPropertyReader;

import java.util.HashMap;
import java.util.Map;

import com.trimble.sms.util.Constants;
import org.glassfish.jersey.server.ServerProperties;

import com.trimble.sms.resources.SmsAdminResource;
import com.trimble.sms.resources.SmsResource;

public class SmsApplication extends ApplicationConfig {

    public SmsApplication() {   
    	register(SmsAdminResource.class);
    	register(SmsResource.class);
    	try {
			String configFile = System
					.getProperty(Constants.CONFIG_FILE_NAME_PROPERTY);

			if (configFile != null && !configFile.isEmpty())
				new JSONPropertyReader().loadObjectFromJSON(configFile);
			else
			new JSONPropertyReader().loadObjectFromJSON();
		} catch (APIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(ServerProperties.MONITORING_STATISTICS_MBEANS_ENABLED, true);
        
        setApplicationName("SmsApplication");
    	addProperties(properties);
    }
}
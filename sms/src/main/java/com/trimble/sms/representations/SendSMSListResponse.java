package com.trimble.sms.representations;

import java.util.*;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
public class SendSMSListResponse  {
  
   private List<SendSMSResponse> sentSMSList = new ArrayList<SendSMSResponse>() ;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("sentSMSList")
  public List<SendSMSResponse> getSentSMSList() {
    return sentSMSList;
  }
  public void setSentSMSList(List<SendSMSResponse> sentSMSList) {
    this.sentSMSList = sentSMSList;
  }


  @Override
  public String toString() {
    return "{" +
            "sentSMSList:" + sentSMSList +
            '}';
  }
}

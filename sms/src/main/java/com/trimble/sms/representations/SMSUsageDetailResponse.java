package com.trimble.sms.representations;

import java.util.ArrayList;
import java.util.List;

public class SMSUsageDetailResponse {
	private int totalMessageCount;
	private double totalAmount;
	private String priceUnit;
	private String appId;
	private String appName;
	private String owner;
	private List<UsagePerMonth> usage;
	
	
	
	public int getTotalMessageCount() {
		return totalMessageCount;
	}



	public void setTotalMessageCount(int totalMessageCount) {
		this.totalMessageCount = totalMessageCount;
	}



	public double getTotalAmount() {
		return totalAmount;
	}



	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}



	public String getPriceUnit() {
		return priceUnit;
	}



	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}



	public String getAppId() {
		return appId;
	}



	public void setAppId(String appId) {
		this.appId = appId;
	}



	public String getAppName() {
		return appName;
	}



	public void setAppName(String appName) {
		this.appName = appName;
	}



	public String getOwner() {
		return owner;
	}



	public void setOwner(String owner) {
		this.owner = owner;
	}



	public List<UsagePerMonth> getUsage() {
		return usage;
	}



	public void setUsage(int messageCount, double amount, String month) {
		if(this.usage==null)
			this.usage=new ArrayList<UsagePerMonth>();
		UsagePerMonth usagePerMonth = new UsagePerMonth();
		usagePerMonth.setAmount(amount);
		usagePerMonth.setMessageCount(messageCount);
		usagePerMonth.setMonth(month);
		this.usage.add(usagePerMonth);
	}



	@Override
	public String toString() {
		return "SMSUsageDetailResponse [totalMessageCount=" + totalMessageCount + ", totalAmount=" + totalAmount
				+ ", priceUnit=" + priceUnit + ", appId=" + appId + ", appName=" + appName + ", owner=" + owner
				+ ", usage=" + usage + "]";
	}



	class UsagePerMonth{
		private int messageCount;
		private double amount;
		private String month;
		public int getMessageCount() {
			return messageCount;
		}
		public void setMessageCount(int messageCount) {
			this.messageCount = messageCount;
		}
		public double getAmount() {
			return amount;
		}
		public void setAmount(double amount) {
			this.amount = amount;
		}
		public String getMonth() {
			return month;
		}
		public void setMonth(String month) {
			this.month = month;
		}
		
	}
}

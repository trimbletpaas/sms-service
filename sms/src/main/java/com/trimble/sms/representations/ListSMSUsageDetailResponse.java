package com.trimble.sms.representations;

import java.util.ArrayList;
import java.util.List;

public class ListSMSUsageDetailResponse {
	List<SMSUsageDetailResponse> usageDetailsByApplication= new ArrayList<SMSUsageDetailResponse>();

	public List<SMSUsageDetailResponse> getUsageDetailsByApplication() {
		if(usageDetailsByApplication==null)
			usageDetailsByApplication=new ArrayList<SMSUsageDetailResponse>();
		return usageDetailsByApplication;
	}

	public void setUsageDetailsByApplication(List<SMSUsageDetailResponse> usageDetailsByApplication) {
		if(this.usageDetailsByApplication==null)
			this.usageDetailsByApplication=new ArrayList<SMSUsageDetailResponse>();
		this.usageDetailsByApplication = usageDetailsByApplication;
	}

	@Override
	public String toString() {
		return "ListSMSUsageDetailResponse [usageDetailsByApplication=" + usageDetailsByApplication + "]";
	}
	
}

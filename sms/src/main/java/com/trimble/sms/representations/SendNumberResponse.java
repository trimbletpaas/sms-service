package com.trimble.sms.representations;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class SendNumberResponse {
	String id;

	@ApiModelProperty(value = "")
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "SendShortcodeResponse [id=" + id + "]";
	}
	
}

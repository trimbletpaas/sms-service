package com.trimble.sms.representations;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "")
public class SendNumberRequest {
	private String appId = null;
	private String number = null;
	private String shortcode = null;
	@ApiModelProperty(value = "")
	@JsonProperty("appId")
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("number")
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("shortcode")
	public String getShortcode() {
		return shortcode;
	}
	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}
	@Override
	public String toString() {
		return "SendNumberRequest [appId=" + appId + ", number=" + number + ", shortcode=" + shortcode + "]";
	}
	
	
	
}

package com.trimble.sms.representations;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class SMSNumberResponse {
	private String appId = null;
	private String number = null;
	private String shortcode = null;
	
	@ApiModelProperty(value = "")
	@JsonProperty("number")
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("appId")
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	@ApiModelProperty(value = "")
	@JsonProperty("shortcode")
	public String getShortcode() {
		return shortcode;
	}
	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}
	@Override
	public String toString() {
		return "SMSShortcodeResponse [appId=" + appId + ", shortcode=" + shortcode + "]";
	}	
}

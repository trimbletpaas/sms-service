package com.trimble.sms.representations;

import javax.validation.constraints.*;
import java.util.*;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;


@ApiModel(description = "")
public class SendSMSRequest  {
  @NotNull(message = "to is required.")
  private List<String> to = new ArrayList<String>() ;
  @NotNull(message = "message is required.") @NotEmpty(message ="message cannot be empty")private String message = null;
   private String mediaURL = null;  
   private boolean useShortCode=false;
   
   @ApiModelProperty(value = "")
   @JsonProperty("useShortCode")
   public boolean getUseShortCode() {
	    return useShortCode;
	  }
	  public void setUseShortCode(boolean useShortCode) {
	    this.useShortCode = useShortCode;
	  }

  
  /**
   * List of numbers the message to be sent to
   **/
  @ApiModelProperty(required = true, value = "List of numbers the message to be sent to")
  @JsonProperty("to")
  public List<String> getTo() {
    return to;
  }
  public void setTo(List<String> to) {
    this.to = to;
  }

  
  /**
   **/
  @ApiModelProperty(required = true, value = "")
  @JsonProperty("message")
  public String getMessage() {
    return message;
  }
  public void setMessage(String message) {
    this.message = message;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("mediaURL")
  public String getMediaURL() {
    return mediaURL;
  }
  public void setMediaURL(String mediaURL) {
    this.mediaURL = mediaURL;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class SendSMSRequest {\n");
    sb.append("  useSortCode: ").append(String.valueOf(useShortCode)).append("\n");
    sb.append("  to: ").append(to).append("\n");
    sb.append("  message: ").append(message).append("\n");
    sb.append("  mediaURL: ").append(mediaURL).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

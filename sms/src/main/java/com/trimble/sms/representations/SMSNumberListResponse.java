package com.trimble.sms.representations;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class SMSNumberListResponse {
	private List<SMSNumberResponse> numbers = new ArrayList<SMSNumberResponse>() ;
	
	@ApiModelProperty(value = "")
	@JsonProperty("numbers")
	public List<SMSNumberResponse> getShortcodes() {
		return numbers;
	}

	public void setShortcodes(List<SMSNumberResponse> shortcodes) {
		this.numbers = shortcodes;
	}

	@Override
	public String toString() {
		return "SMSNumberListResponse [numbers=" + numbers + "]";
	}
	
	
}

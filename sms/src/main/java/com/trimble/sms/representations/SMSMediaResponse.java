package com.trimble.sms.representations;


import io.swagger.annotations.*;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
public class SMSMediaResponse  {
  
   private String id = null;
   private String dateCreated = null;
   private String dateUpdated = null;
   private String smsId = null;
   private String contentType = null;
   private String mediaURL = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("dateCreated")
  public String getDateCreated() {
    return dateCreated;
  }
  public void setDateCreated(String dateCreated) {
    this.dateCreated = dateCreated;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("dateUpdated")
  public String getDateUpdated() {
    return dateUpdated;
  }
  public void setDateUpdated(String dateUpdated) {
    this.dateUpdated = dateUpdated;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("smsId")
  public String getSmsId() {
    return smsId;
  }
  public void setSmsId(String smsId) {
    this.smsId = smsId;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("contentType")
  public String getContentType() {
    return contentType;
  }
  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("mediaURL")
  public String getMediaURL() {
    return mediaURL;
  }
  public void setMediaURL(String mediaURL) {
    this.mediaURL = mediaURL;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class SMSMediaResponse {\n");
    
    sb.append("  id: ").append(id).append("\n");
    sb.append("  dateCreated: ").append(dateCreated).append("\n");
    sb.append("  dateUpdated: ").append(dateUpdated).append("\n");
    sb.append("  smsId: ").append(smsId).append("\n");
    sb.append("  contentType: ").append(contentType).append("\n");
    sb.append("  mediaURL: ").append(mediaURL).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

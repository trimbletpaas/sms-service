package com.trimble.sms.representations;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class SMSUsageResponse {
	private int messageCount;
	private double price;
	private String priceUnit;
	private String startDate;
	private String endDate;
	private String appId;
	private String appName;
	private String owner;
	
	@ApiModelProperty(value = "")
	@JsonProperty("appId")
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("appName")
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("owner")
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("startDate")
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("endDate")
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("messageCount")
	public int getMessageCount() {
		return messageCount;
	}
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("price")
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@ApiModelProperty(value = "")
	@JsonProperty("priceUnit")
	public String getPriceUnit() {
		return priceUnit;
	}
	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}
	@Override
	public String toString() {
		return "SMSUsageResponse [messageCount=" + messageCount + ", price=" + price + ", priceUnit=" + priceUnit + "]";
	}
	
	
}

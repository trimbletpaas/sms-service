package com.trimble.sms.representations;

import java.util.*;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
public class SMSListResponse  {
  
   private List<SMSDetailResponse> smsList = new ArrayList<SMSDetailResponse>() ;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("smsList")
  public List<SMSDetailResponse> getSmsList() {
    return smsList;
  }
  public void setSmsList(List<SMSDetailResponse> smsList) {
    this.smsList = smsList;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class SMSListResponse {\n");
    
    sb.append("  applications: ").append(smsList).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

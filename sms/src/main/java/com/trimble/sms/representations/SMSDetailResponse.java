package com.trimble.sms.representations;


import io.swagger.annotations.*;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.simple.JSONObject;


@ApiModel(description = "")
public class SMSDetailResponse  {
  
   private String id = null;
   public enum TypeEnum {
     Incoming,  Outgoing, 
  };
   private TypeEnum type = null;
   private String dateCreated = null;
   private String dateUpdated = null;
   private String dateSent = null;
   private String to = null;
   //private String from = null;
   private String message = null;
   private String status = null;
   private Integer numberOfmedia = null;
   private Integer errorCode = null;
   private String errorMessage = null;
   private String sentUsing = null;
   
   /**
    **/
   @ApiModelProperty(value = "")
   @JsonProperty("sentUsing")
  public String getSentUsing() {
	return sentUsing;
}
public void setSentUsing(String sentUsing) {
	this.sentUsing = sentUsing;
}
/**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("type")
  public TypeEnum getType() {
    return type;
  }
  public void setType(TypeEnum type) {
    this.type = type;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("dateCreated")
  public String getDateCreated() {
    return dateCreated;
  }
  public void setDateCreated(String dateCreated) {
    this.dateCreated = dateCreated;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("dateUpdated")
  public String getDateUpdated() {
    return dateUpdated;
  }
  public void setDateUpdated(String dateUpdated) {
    this.dateUpdated = dateUpdated;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("dateSent")
  public String getDateSent() {
    return dateSent;
  }
  public void setDateSent(String dateSent) {
    this.dateSent = dateSent;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("to")
  public String getTo() {
    return to;
  }
  public void setTo(String to) {
    this.to = to;
  }

  
 /* *//**
   **//*
  @ApiModelProperty(value = "")
  @JsonProperty("from")
  public String getFrom() {
    return from;
  }
  public void setFrom(String from) {
    this.from = from;
  }
*/
  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("message")
  public String getMessage() {
    return message;
  }
  public void setMessage(String message) {
    this.message = message;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("status")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("numberOfmedia")
  public Integer getNumberOfmedia() {
    return numberOfmedia;
  }
  public void setNumberOfmedia(Integer numberOfmedia) {
    this.numberOfmedia = numberOfmedia;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("errorCode")
  public Integer getErrorCode() {
    return errorCode;
  }
  public void setErrorCode(Integer errorCode) {
    this.errorCode = errorCode;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("errorMessage")
  public String getErrorMessage() {
    return errorMessage;
  }
  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }


    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", dateCreated='" + dateCreated + '\'' +
                ", dateUpdated='" + dateUpdated + '\'' +
                ", dateSent='" + dateSent + '\'' +
                ", to='" + to + '\'' +
                ", message='" + JSONObject.escape(message) + '\'' +
                ", status='" + status + '\'' +
                ", numberOfmedia=" + numberOfmedia +
                ", errorCode=" + errorCode +
                ", errorMessage='" + JSONObject.escape(errorMessage) + '\'' +
                ", sentUsing='" + sentUsing + '\'' +
                '}';
    }
}

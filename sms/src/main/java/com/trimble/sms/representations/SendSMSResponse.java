package com.trimble.sms.representations;


import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.simple.JSONObject;


@ApiModel(description = "")
public class SendSMSResponse  {
  
   //private String from = null;
   private String to = null;
   private String id = null;
   private String status = null;
   private Integer errorCode = null;
   private String errorMessage = null;

  
//  /**
//   **/
//  @ApiModelProperty(value = "")
//  @JsonProperty("from")
//  public String getFrom() {
//    return from;
//  }
//  public void setFrom(String from) {
//    this.from = from;
//  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("to")
  public String getTo() {
    return to;
  }
  public void setTo(String to) {
    this.to = to;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("id")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("status")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("errorCode")
  public Integer getErrorCode() {
    return errorCode;
  }
  public void setErrorCode(Integer errorCode) {
    this.errorCode = errorCode;
  }
  
  @ApiModelProperty(value = "")
  @JsonProperty("errorMessage")
  public String getErrorMessage() {
    return errorMessage;
  }
  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }


  @Override
  public String toString() {
    return JSONObject.escape("{" +
            "to='" + to + '\'' +
            ", id='" + id + '\'' +
            ", status='" + status + '\'' +
            ", errorCode=" + errorCode +
            ", errorMessage='" + JSONObject.escape(errorMessage) + '\'' +
            '}');
  }
}

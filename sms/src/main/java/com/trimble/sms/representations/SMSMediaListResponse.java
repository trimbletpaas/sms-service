package com.trimble.sms.representations;

import java.util.*;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
public class SMSMediaListResponse  {
  
   private List<SMSMediaResponse> mediaList = new ArrayList<SMSMediaResponse>() ;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("mediaList")
  public List<SMSMediaResponse> getMediaList() {
    return mediaList;
  }
  public void setMediaList(List<SMSMediaResponse> applications) {
    this.mediaList = applications;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class SMSMediaListResponse {\n");
    
    sb.append("  mediaList: ").append(mediaList).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

import java.util.Timer;
import java.util.TimerTask;

import com.trimble.sms.util.Constants;
import org.hibernate.HibernateException;

import com.trimble.apiutil.exception.APIException;
import com.trimble.apiutil.reader.JSONPropertyReader;
import com.trimble.sms.util.DatabaseUtil;
import com.trimble.sms.util.TwilioAPIUtil;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Date;

public final class Scheduler extends TimerTask {

  /** Construct and use a TimerTask and Timer. */
  public static void main (String... arguments ) {
    TimerTask fetchMail = new Scheduler();
    Timer timer = new Timer();
	  try{
    timer.scheduleAtFixedRate(fetchMail, getEndOfDay(), fONCE_PER_DAY);
	  }
	  catch(Exception e){
	    	e.printStackTrace();
	    	timer.scheduleAtFixedRate(fetchMail, getEndOfDay(), fONCE_PER_DAY);
	    }
  }

  /**
  * Implements TimerTask's abstract run method.
  */
  @Override public void run(){
    try {
      String configFile = System
              .getProperty(Constants.CONFIG_FILE_NAME_PROPERTY);
      if (configFile != null && !configFile.isEmpty())
        new JSONPropertyReader().loadObjectFromJSON(configFile);
      else
        new JSONPropertyReader().loadObjectFromJSON();
    	List<String> ids = new DatabaseUtil().getApplicationIds();
    	for(String id:ids){
    		System.out.println(id);
    		new TwilioAPIUtil().getUsageToday(id);
    	}
	} catch (HibernateException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (APIException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
  }
  
  

  // PRIVATE

  //expressed in milliseconds
  private final static long fONCE_PER_DAY = 1000*60*60*24;

  private final static int fONE_DAY = 0;
  private final static int fFOUR_AM = 15;
  private final static int fZERO_MINUTES = 24;
  

  private static Date getEndOfDay(){
    Calendar tomorrow = new GregorianCalendar();
    tomorrow.add(Calendar.DATE, fONE_DAY);
    Calendar result = new GregorianCalendar(
      tomorrow.get(Calendar.YEAR),
      tomorrow.get(Calendar.MONTH),
      tomorrow.get(Calendar.DATE),
      fFOUR_AM,
      fZERO_MINUTES
    );
    System.out.println(result.getTime());
    return result.getTime();
  }
}
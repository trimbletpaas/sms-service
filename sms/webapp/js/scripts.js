$("document").ready(function() {
	$.ajaxSetup({
		contentType: "application/json"
	});
	
	$("#config").text(JSON.stringify(config, undefined, 4));
	$("#result").hide();
	$(".alert").hide();
	var template = $('#services-template').html();
    var info = Mustache.to_html(template, {"services": services});
    $('#servicesList').html(info);
    
    $("#run").click(function(){
    	editedConfig = JSON.parse($("#config").text());
    	var suite = new Suite(editedConfig, window.location.origin);
    	suite.printConfig();
    	suite.run();
    	
    });
});
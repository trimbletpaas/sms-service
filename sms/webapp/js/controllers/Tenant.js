var Tenant = function(tenant, suite) {
	this.tenant = tenant;
	this.suite = suite;
	this.serviceName = "tenant";
	this.deferred =  $.Deferred();
} 

Tenant.prototype.run = function() {
	var obj = this;
	$.ajax({
		type: "POST",
		url: obj.suite.baseUrl+obj.serviceName,
		data: JSON.stringify(obj.tenant),
		beforeSend: function() {
			obj.suite.showLoading(obj.serviceName);
		},
		success: function(data) {
			obj.deferred.resolve(data);
		}
	});
	this.deferred.then(
		function(data){
			obj.suite.successCallback(data, obj.serviceName);
		}, 
		function(data){
			obj.suite.errorCallback(data, obj.serviceName);
		}).always(function() { obj.suite.hideLoading("tenant") });
}
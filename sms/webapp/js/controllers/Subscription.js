var Subscription = function(subscription, suite) {
	this.subscription = subscription;
	this.suite = suite;
	this.serviceName = "subscribe";
	this.deferred =  $.Deferred();
} 

Subscription.prototype.run = function() {
	var obj = this;
	var subscribeApi =  $.ajax({
		type: "POST",
		url: obj.suite.baseUrl + obj.serviceName,
		data: JSON.stringify(obj.subscription),
		beforeSend: function() {
			obj.suite.showLoading(obj.serviceName);
		},
		success: function(data) {
			obj.deferred.resolve(data);
		}
	});
	this.deferred.then(
		function(data){
			obj.suite.successCallback(data, obj.serviceName);
		}, 
		function(data){
			obj.suite.errorCallback(data, obj.serviceName);
		}).always(function() { obj.suite.hideLoading(obj.serviceName) });
}
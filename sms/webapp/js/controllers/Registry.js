var Registry = function(registry, suite) {
	this.registry = registry;
	this.suite = suite;
	this.serviceName = "registry";
	this.deferred =  $.Deferred();
} 

Registry.prototype.run = function() {
	var obj = this;
	var registryInstall =  $.ajax({
		type: "POST",
		url: obj.suite.baseUrl + obj.serviceName,
		data: JSON.stringify(obj.registry),
		beforeSend: function() {
			obj.suite.showLoading(obj.serviceName);
		},
		success: function(data) {
			obj.deferred.resolve(data);
		}
	});
	this.deferred.then(
		function(data){
			obj.suite.successCallback(data, obj.serviceName);
		}, 
		function(data){
			obj.suite.errorCallback(data, obj.serviceName);
		}).always(function() { obj.suite.hideLoading(obj.serviceName) });
}
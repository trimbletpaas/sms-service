var Model = function(config) {
	this.config = config;
};

Model.prototype.tenant = function() {
	return {
		firstName: this.config.firstName,
		lastName: this.config.lastName,
		email: this.config.email,
		domainName: this.config.domainName,
		usagePlan: this.config.usagePlan,
		tenantAdminName:this.config.tenantAdminName,
		tenantAdminPassword: this.config.tenantAdminPassword,
		username: this.config.username,
		password: this.config.password,
		servicesUrl: this.config.servicesUrl
	}
};
Model.prototype.registry = function() {
	return {
		repositoryName: this.config.repositoryName,
		featureName: this.config.featureName,
		featureVersion: this.config.featureVersion,
		repositoryLocation: this.config.repositoryLocation,
		username: this.config.username,
		password: this.config.password,
		servicesUrl: this.config.servicesUrl
	}
};
Model.prototype.subscription = function() {
	return {
		storeUrl: this.config.storeUrl,
		applicationname: this.config.applicationname,
		tier: this.config.applicationname,
		apiname: this.config.apiname,
		version: this.config.version,
		username: this.config.username,
		password: this.config.password,
		servicesUrl: this.config.servicesUrl
	}
};
Model.prototype.toString = function(obj){
	return JSON.stringify(obj);
}
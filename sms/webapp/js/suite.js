var Suite = function(config, baseUrl) {
	this.config = config;
	this.baseUrl = baseUrl + "/tpasspostdeployment/services/";
	this.model = new Model(this.config);
	
}
Suite.prototype.run = function() { 
	var obj = this;
	var createTenant = new Tenant(this.model.tenant(), this);
	var subscribeApi = new Subscription(obj.model.subscription(), this);
	var registryInstall = new Registry(obj.model.registry(), this);
	createTenant.deferred.done(function(data){if(data.status){subscribeApi.run()}});
	subscribeApi.deferred.done(function(data){if(data.status){registryInstall.run()}});
	createTenant.run();
}
Suite.prototype.printConfig = function() {
	console.log(this.config);
}
Suite.prototype.hideLoading = function(element) {
	$("#loading-" + element).hide();
	$("#" + element).removeClass("panel-warning");
}
Suite.prototype.showLoading = function(element) {
	$("#loading-" + element).show();
	$("#" + element).addClass("panel-warning");
}
Suite.prototype.showSuccess = function(element) {
	$("#" + element).removeClass("panel-danger");
	$("#" + element).addClass("panel-success");
}
Suite.prototype.showError = function(element) {
	$("#" + element).removeClass("panel-success");
	$("#" + element).addClass("panel-danger");
	$("#alertBox").show();
}
Suite.prototype.setMessage = function(element, message) {
	$("#collapse" + element +" .panel-body").html(message);
}
Suite.prototype.successCallback= function(data, element) {
	if(data.status) {
		this.showSuccess(element);
		this.setMessage(element, data.message);
	}
	else {
		this.showError(element);
		this.setMessage(element, data.message);
		$("#alertMessage").html(data.message);
	}
}
Suite.prototype.errorCallback = function(data, element) {
	this.showError(element);
	this.setMessage(element, data.statusMessage);
	$("#alertMessage").html(data.statusMessage);
}

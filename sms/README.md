# Swagger Generated Jax-rs application 

## Overview
This application is auto-generated through TPaaS-Swagger Code generation utility based on the swagger specification used.

## Supported features
Swagger Documentation 
Validation (Java Bean validation)
Exception handling
Logging
Environmental configurations
JWT Injection
Monitoring & Tracing
Resource to serve current build version
Emailing capability supporting templates

## Prerequisites
setenv.sh or setenv.bat to be configured with expected input and be included in the server startup
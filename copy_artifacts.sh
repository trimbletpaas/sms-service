#!/bin/bash
#Author:Ramesh Muthuraj
#Version: 1.0
cd sms
version=`grep -i '<version>' pom.xml | head -1 | awk -F'>' '{print $2}' | awk -F'<' '{print $1}'`
cd ..
snapshot_server="http://artifactory.trimblepaas.com/artifactory/libs-snapshot-local/com/trimble/sms"
release_server="http://artifactory.trimblepaas.com/artifactory/libs-release-local/com/trimble/sms"

#gzip devops
echo "gzipping devops dir.."	
zip -r devops.zip devops

#copy it to artifactory

grep -i '<version>' sms/pom.xml | head -1 | awk -F'>' '{print $2}' | awk -F'<' '{print $1}' | grep -i "snapshot"
stat=$(echo $?)
if [ $stat -eq 0 ]; then
        curl -T devops.zip $snapshot_server/$version/ --user tpaas:yMGfBCF7n36ujKbc
else
        curl -T devops.zip $release_server/$version/ --user tpaas:yMGfBCF7n36ujKbc
fi

echo "Completed"
